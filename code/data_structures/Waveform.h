/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WAVEFORM_H
#define WAVEFORM_H
#include "Baseline.h"
#include "TF1.h"
#include "TH1.h"
#include "TROOT.h"
#include <iostream>

/**
 * A Waveform class that inherits from ROOT's TH1D class.
 *
 * The Waveform contains a Baseline object that can be set after creation.
 */
class Waveform : public TH1D {
public:
  Baseline getWaveformBaseline();
  void setWaveformBaseline(Baseline baseline);

  Waveform() {}
  Waveform(int aNBin, double aMin, double aMax, const char *aName = 0);
  Waveform(const TH1D &aWF, const char *aName = 0);
  virtual ~Waveform();

  ClassDef(Waveform, 1);

private:
  Baseline waveformBaseline; /*!< Global Baseline for the Waveform. */
};

#endif // WAVEFORM_H
