/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Channel.h"
#include "Pulse.h"

/**
 * Channel constructor.
 *
 * Takes a Waveform as the parameter and sets the member variable, `wf`.
 */
Channel::Channel(Waveform *newWaveform, int chID) : chID(chID) {
  wf = newWaveform;
}

Channel::~Channel() {}

/**
 * Store the pulses found.
 *
 * Takes a vector of Pulse as the parameter and sets the member variable,
 * `pulses`. This does not append to the member variable, `pulses` but replaces
 * it with the paramenter.
 */
void Channel::setPulses(std::vector<Pulse> pulses) { this->pulses = pulses; }

/**
 * Return current waveform.
 *
 * Returns the member variable, `wf` when called.
 */
Waveform *Channel::getWaveform() { return wf; }

void Channel::setWaveform(Waveform *wf) { this->wf = wf; }

/**
 * Return the pulses found.
 *
 * Returns member vector, `pulses` as an address reference when called.
 * This allows pass by reference and changes to the pulses outside of the
 * Channel class will persist.
 */
std::vector<Pulse> *Channel::getPulses() { return &pulses; }

/**
 * Returns if the waveform is valid for the channel (polarity and timeing for
 * example)
 *
 * Returns the member variable 'isVaild'
 */
bool Channel::getValid() { return isValid; };

/**
 * Set the if the processed waveform is valid (polarity and timeing for example)
 * for the channel.
 *
 * Takes a bool as a parameter (Deafult true) and sets the member variable,
 * `isVaild`.
 */
void Channel::setValid(bool isValid) { this->isValid = isValid; };

/**
 * Returns the ID of the channel zero indexed from from the start of the midas
 * file
 */
int Channel::getChID() { return chID; }