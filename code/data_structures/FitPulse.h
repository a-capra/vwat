/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FITPULSE_H
#define FITPULSE_H

#include "TObject.h"

/**
 * A class that represents a single pulse after fitting.
 */
class FitPulse : public TObject {
public:
  FitPulse()
      : lowLimit(0), highLimit(0), baseline(0), time(0), amp(0), riseTime(0),
        fallTime(0), time2Frac(0), fallTime2(0), chi2(0), NDF(0), charge(0){};
  virtual ~FitPulse() {}
  double lowLimit;
  double highLimit;
  double baseline;
  double time;
  double amp;
  double riseTime;
  double fallTime;
  double time2Frac;
  double fallTime2;
  double chi2;
  double NDF;
  double charge;

  ClassDef(FitPulse, 1);
};

#endif // FITPULSE_H
