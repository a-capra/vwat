/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PULSE_H
#define PULSE_H

#include "FitPulse.h"
#include "TObject.h"

/**
 * A class that represents a single pulse.
 *
 * Has member variables, time, absolute amplitude, amplitude, baseline,
 * charge, width, singlePulseTemplateChi2 and a FitPulse.
 */
class Pulse : public TObject {
public:
  Pulse(double aTime, double aAbsAmp, double aAmp, double aBaseline,
        double aCharge, int aWidth, double aSinglePulseTemplateChi2,
        FitPulse aFit)
      : time(aTime), absAmp(aAbsAmp), amp(aAmp), baseline(aBaseline),
        charge(aCharge), width(aWidth),
        singlePulseTemplateChi2(aSinglePulseTemplateChi2), fit(aFit){};
  Pulse(Pulse *pulse)
      : time(pulse->time), absAmp(pulse->absAmp), amp(pulse->amp),
        baseline(pulse->baseline), charge(pulse->charge), width(pulse->width),
        singlePulseTemplateChi2(pulse->singlePulseTemplateChi2),
        fit(pulse->fit){};
  Pulse() {}
  virtual ~Pulse() {}
  double time;
  double absAmp;
  double amp;
  double baseline;
  double charge;
  int width;
  double singlePulseTemplateChi2; // TODO: if we don't want the template check
                                  // this can be removed

  FitPulse fit;

  ClassDef(Pulse, 1);
};

#endif // PULSE_H
