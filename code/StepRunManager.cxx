/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StepRunManager.h"
#include "TROOT.h"
#include <memory>

/**
 * StepRunManager constructor
 */
StepRunManager::StepRunManager(int eventID) : eventID(eventID) {}

/**
 * Call the entires processing pipeline
 */
void StepRunManager::processEvent() {
  pulseFinding();
  pulseAnalysis();
}

/**
 * Calls the baseline extraction and pulse finding processor
 * Prepares the event to be fit
 *
 * Used to seaprate pulseFinding from PulseFitting in root macros
 */
void StepRunManager::pulseFinding() {
  flProcessor.processEvent();
  bsProcessor.processEvent();
  pfProcessor.processEvent();
}

/**
 * Calls the baseline extraction and pulse finding processor
 * Prepares the event to be fit
 *
 * Used to seaprate pulseFinding from PulseFitting in root macros
 */
void StepRunManager::pulseAnalysis() { paProcessor.processEvent(); }

/**
 * Start the run manager by initializes the Processors from the config and
 * interating to the first pulse
 */
void StepRunManager::start() {
  reader = createReader(currEvent);
  flProcessor = FilterProcessor(channelConfigs, currEvent);
  bsProcessor = BaselineProcessor(channelConfigs, currEvent);
  pfProcessor = PulseFindingProcessor(channelConfigs, currEvent);
  paProcessor = PulseAnalysisProcessor(channelConfigs, currEvent);

  currentPulseIndex = 0;
  while (!(reader->isDone()) && currentPulseIndex < eventLimit) {
    if (currentPulseIndex == eventID)
      break;
    currentPulseIndex++;
    reader->next();
  }
}

/**
 * Advances the StepRunManager to the next event
 * Returns false is there are no more events
 */
bool StepRunManager::next() {
  if (reader->isDone() || currentPulseIndex >= eventLimit)
    return false;
  reader->next();
  return true;
}

TH1D *StepRunManager::getFilter(int ch) { return flProcessor.getFilter(ch); }

/**
 * Returns the pulses associated with the current event for the specified
 * channel Input parameter is the target channel id
 */
std::vector<Pulse> *StepRunManager::getPulse(int chID) {
  return currEvent->getChannels()->at(chID).getPulses();
}

/**
 * Returns the waveform associated with the current event for the specified
 * channel Input parameter is the target channel id
 */
Waveform *StepRunManager::getWaveform(int chID) {
  return currEvent->getChannels()->at(chID).getWaveform();
}

/**
 * Returns the channel associated with the current event for the specified
 * channel Input parameter is the target channel id
 */
Channel *StepRunManager::getChannel(int chID) {
  return &currEvent->getChannels()->at(chID);
}

TF1 *StepRunManager::getFitFunction(Pulse &p, int chID) {
  TF1 *fit = paProcessor.getFitFunction(p, chID);
  return fit;
}

void StepRunManager::setFitFunction(std::vector<Pulse> *pulses, Waveform *wf,
                                    TF1 *fitFunction, int chID) {
  paProcessor.setFitFunction(pulses, wf, fitFunction, chID);
}

/**
 * A simple helper function that draws a TGraph of the input 'pulses' and prints
 * there basic values
 */
void RootHelper::drawPulses(std::vector<Pulse> *pulses) {
  int pulseID = 0;
  for (Pulse p : *pulses) {
    if (p.time ==
        0) // checking if the pulse came from pulse finding for refitting
      continue;
    std::cout << "Pulse Information (No FIT): " << pulseID << std::endl;
    std::cout << " TIME: " << p.time << " CHARGE: " << p.charge
              << " ABS. AMPLITUDE: " << p.absAmp << std::endl;
    std::cout << " AMPLITUDE: " << p.amp << std::endl;
    pulseID++;
  }

  TGraph *GPulse = RootHelper::getPulseGraph(pulses);
  GPulse->SetMarkerStyle(23);
  GPulse->SetMarkerColor(2);
  GPulse->Draw("p");
}

/**
 * Returns a fit for all of the pluses for a waveform
 * Takes in the pulses and a waveform to use as the bounds
 * Retusn a TF1 pointer containing all the points (upto the max allowed by the
 * fit function)
 */
TF1 *RootHelper::getFitFunction(std::vector<Pulse> *pulses, Waveform *wf,
                                TF1 *fitFunction) {
  int maxNumPulsesFit = 15; // TODO: this should be from the config instead
  int tNFitPulse;
  if (pulses->size() > maxNumPulsesFit) {
    std::cout << "WARNING, number of pulses exceed max allowed for function "
              << pulses->size() << " vs " << maxNumPulsesFit << " allowed"
              << std::endl;
    tNFitPulse = maxNumPulsesFit;
  } else {
    tNFitPulse = pulses->size();
  }

  fitFunction->ReleaseParameter(5);
  fitFunction->FixParameter(5, tNFitPulse);
  // include all pulses and full range of the waveform
  fitFunction->SetLineColor(2);
  fitFunction->SetRange(wf->GetXaxis()->GetXmin(), wf->GetXaxis()->GetXmax());
  for (int i = 0; i < tNFitPulse; i++) {
    fitFunction->SetParameter(6 + 2 * (i), pulses->at(i).fit.amp);
    fitFunction->SetParameter(7 + 2 * (i), pulses->at(i).fit.time);
    std::cout << "Fit Function: "
              << "PulseID: " << i << " Time: " << pulses->at(i).fit.time
              << " Amp: " << pulses->at(i).fit.amp
              << " Chi2/NDF: " << pulses->at(i).fit.chi2 / pulses->at(i).fit.NDF
              << std::endl;
  }

  for (int i = tNFitPulse; i < maxNumPulsesFit; i++) {
    fitFunction->FixParameter(6 + 2 * (i), 0);
    fitFunction->FixParameter(7 + 2 * (i), 0);
  }
  return fitFunction;
}

/**
 * Returns a TGraph of the input pulses amplitude vs the pulses time
 * Takes a vector of pulses
 * Returns a new TGraph containing the amp and time of each pulses
 */
TGraph *RootHelper::getPulseGraph(std::vector<Pulse> *pulses) {
  std::vector<double> t;
  std::vector<double> absAmp;
  for (Pulse pulse : *pulses) {
    if (pulse.absAmp == 0.0 ||
        pulse.time ==
            0.0) // checking if the pulse came from pulse finding for refitting
      continue;
    t.push_back(pulse.time);
    absAmp.push_back(pulse.absAmp);
  }
  TGraph *tG = new TGraph(pulses->size(), t.data(), absAmp.data());
  return tG;
}
