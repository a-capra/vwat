/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WAVEFORM_STATISTIC_H
#define WAVEFORM_STATISTIC_H

double FuncExpGausMulti(double *x, double *p);

double FuncExp2GausMulti(double *x, double *p);

double FuncExpRiseMulti(double *x, double *p);

double FuncCR_RC(double *xs, double *par);

double FuncCR_RCMulti(double *xs, double *par);

void bitrv2(int n, double *a);

void cdft(int n, double wr, double wi, double *a);

void rdft(int n, double wr, double wi, double *a);

void ddct(int n, double wr, double wi, double *a);

void ddst(int n, double wr, double wi, double *a);

void bitrv(int n, double *a);

void dfct(int n, double wr, double wi, double *a);

void dfst(int n, double wr, double wi, double *a);

#endif // WAVEFORM_STATISTIC_H
