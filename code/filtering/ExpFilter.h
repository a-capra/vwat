/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __EXPFILTERSTRATEGY_H
#define __EXPFILTERSTRATEGY_H

#include "../data_structures/Channel.h"
#include "FilterStrategy.h"
#include <jsoncpp/json/json.h>

/**
 * A simple pulse filter strategy. Each waveform follows the exp recursive exp
 * filter formula. The strength of the filter is controlled by the config value
 * `SmoothingFactor`
 */
class ExpFilterStrategy : public IFilterStrategy {
public:
  ExpFilterStrategy();
  void filter(Channel &channel, Json::Value cfg);
  std::vector<std::string> checkParams(Json::Value params);
};

#endif // __EXPFILTERSTRATEGY_H
