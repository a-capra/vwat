/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ExpFilter.h"
#include "TROOT.h"

ExpFilterStrategy::ExpFilterStrategy() {}

/*
 * Filters the waveform using the recursive formula s_n = smoothingFactor*x_n +
 * (1-smoothingFactor)*x_(n-1) for some waveform x_n
 */
void ExpFilterStrategy::filter(Channel &channel, Json::Value cfg) {

  double smoothingFactor = cfg["SmoothingFactor"].asDouble();

  Waveform *wf = channel.getWaveform();
  int nBins = wf->GetNbinsX();

  for (int i = 2; i < nBins;
       i++) { // The first bin of the waveform seems to be always zero
    double s = smoothingFactor * wf->GetBinContent(i) +
               (1 - smoothingFactor) * wf->GetBinContent(i - 1);
    wf->SetBinContent(i, s);
  }
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the algorithm depends on.
 */
std::vector<std::string> ExpFilterStrategy::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {"SmoothingFactor"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
