/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ExpMatchFilter.h"

/**
 * Required call the the super constructor to read values from the config.
 * Also call the createPulseShapeKernel to inital the convolution kernel.
 */
ExpMatchFilterStrategy::ExpMatchFilterStrategy(Json::Value cfg)
    : MatchedFilterStrategy::MatchedFilterStrategy(cfg) {
  this->createPulseShapeKernel(cfg);
}

/**
 * Creates the ROOT TF1 fitting function that is in the form of a exp.
 */
void ExpMatchFilterStrategy::createPulseShapeFunction(Json::Value cfg) {
  double T = cfg["FallTimeTau"].asDouble();
  this->pulseShape = new TF1("pulseShape", "exp(-[0]*x)", 0, INT32_MAX);
  this->pulseShape->SetParameter(0, 1 / T); //,-400.0,-20000,909);
}