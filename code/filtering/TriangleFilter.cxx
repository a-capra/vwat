/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TriangleFilter.h"

/**
 * Genrerates the normalized triangular kernel
 */
TriangleFilter::TriangleFilter(Json::Value cfg)
    : ConvolutionFilterStrategy(cfg) {
  kernel = new double[kernelSize];

  int kernel_offset = (kernelSize - 1) / 2;

  // init kernel
  kernel[kernel_offset] = kernel_offset + 1;
  double mag = kernel_offset + 1;
  for (int i = 0; i < (kernelSize - 1) / 2; i++) {
    kernel[i] = i + 1;
    kernel[kernelSize - i - 1] = i + 1;
    mag += kernel[i] + kernel[kernelSize - i - 1];
  }

  // normalize
  for (int i = 0; i < kernelSize; i++) {
    kernel[i] /= mag;
  }
}
