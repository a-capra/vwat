/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CONVOLUTIONFILTERSTRATEGY_H
#define __CONVOLUTIONFILTERSTRATEGY_H

#include "../data_structures/Channel.h"
#include "FilterStrategy.h"
#include <jsoncpp/json/json.h>

/**
 * A general implementation of the IFilterStrategy the preforms a discret
 * 'convolution' of the kernel and the waveform. By convention it assumes the
 * kernel is already time reverased. This is not an optimized implementation and
 * has time poor time complexity. Look into fast convolution algorithms using
 * FFT's for better performance.
 */
class ConvolutionFilterStrategy : public IFilterStrategy {
public:
  ConvolutionFilterStrategy(Json::Value cfg);
  void filter(Channel &channel, Json::Value cfg);
  std::vector<std::string> checkParams(Json::Value params);
};

#endif // __CONVOLUTIONFILTERSTRATEGY_H
