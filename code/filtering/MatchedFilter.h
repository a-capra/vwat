/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __MATCHEDFILTERSTRATEGY_H
#define __MATCHEDFILTERSTRATEGY_H

#include "../data_structures/Channel.h"
#include "ConvolutionFilterStrategy.h"
#include <jsoncpp/json/json.h>

/**
 * The match filter is a common filter in signal processing since it provids
 * good S/N if the singals shape is know In this implimentation the
 * createPulseShapeFunction creates a TF1 fuction that is time reveased and
 * discretized in createPulseShapeKernel This kernel is then run across the
 * waveform as seen in ConvolutionFilterStrategy. Note that the discretization
 * for some function f(x) starts at x=0
 */
class MatchedFilterStrategy : public ConvolutionFilterStrategy {
public:
  MatchedFilterStrategy(Json::Value cfg);
  ~MatchedFilterStrategy();
  void createPulseShapeKernel(Json::Value cfg);
  virtual void createPulseShapeFunction(Json::Value cfg) = 0;

protected:
  TF1 *pulseShape;
};

#endif // __MATCHEDFILTERSTRATEGY_H
