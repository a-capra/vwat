/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TrapezoidalFilter.h"
#include "TROOT.h"
#include "TString.h"

TrapezoidalFilter::TrapezoidalFilter() {}

/*
 * Unpacks the 'channel's Waveform and sets its waveform baseline value.
 * Return not implemented
 */
void TrapezoidalFilter::filter(Channel &channel, Json::Value cfg) {

  int l_sum = cfg["KernelSize"].asInt();
  int l_delay = cfg["l_delay"].asInt();

  Waveform *h = channel.getWaveform();

  //
  // Apply trapezoidal filter with a delay ( lag ) of  l_delay  and box width
  // l_sum  over histogram  h  using a baseline of  base_line  . Return result
  // in cloned histogram with name set to  name  and title set to  title . If no
  // name is given use original name with "_trap" appended.
  //

  int l, l_max;
  double s, s_prev;
  int nBins = h->GetNbinsX();

  Waveform *h_t;

  TString p = h->GetName() + TString("_trap");
  double base_line = h->getWaveformBaseline().mu;

  l_max = h->GetXaxis()->GetNbins();
  h_t = (Waveform *)h->Clone(p);
  s_prev = base_line * l_sum;

  s = 0;
  for (l = 0; l < l_sum; ++l)
    s += h->GetBinContent(l + 1);

  for (l = 0; l < l_max; ++l) {

    h_t->SetBinContent(l + 1, (s - s_prev) / l_sum + base_line);

    if (l < l_delay) {
      s_prev -= base_line;
    } else {
      s_prev -= h->GetBinContent(l - l_delay + 1);
    }
    if (l < l_delay - l_sum) {
      s_prev += base_line;
    } else {
      s_prev += h->GetBinContent(l - l_delay + l_sum + 1);
    }

    if (l < l_max - l_sum) {
      s += h->GetBinContent(l + l_sum + 1);
    } else {
      s += base_line;
    }
    s -= h->GetBinContent(l + 1);
  }

  for (int i = 0; i < nBins; i++) {
    h->SetBinContent(i, h_t->GetBinContent(i));
  }

  h_t->Delete();
}
/**
 * Checks the runtime parameters and returns a boolean indicating whether or not
 * they contain every value that the algorithm depends on. It's a bit ugly, but
 * pre-checking those parameters lets the program fail fast.
 */
std::vector<std::string> TrapezoidalFilter::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {"KernelSize", "l_delay"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
