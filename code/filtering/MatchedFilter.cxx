/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MatchedFilter.h"
#include "TROOT.h"

/**
 * MatchedFilterStrategy constructor, calls parent (ConvolutionFilterStrategy)
 * constructor.
 */
MatchedFilterStrategy::MatchedFilterStrategy(Json::Value cfg)
    : ConvolutionFilterStrategy(cfg) {}

/**
 * Matched Filter Destructor.
 * Deletes pulseShape.
 */
MatchedFilterStrategy::~MatchedFilterStrategy() { delete pulseShape; }

/**
 * Creates the kernel that will be run across the signal by sampling the signal
 * shape function 'pulseShape' The size of the filter is defined by the
 * `kernelSize` parameter
 */
void MatchedFilterStrategy::createPulseShapeKernel(Json::Value cfg) {
  double bin_size = 2.0; // TODO: is this read in by the reader of should it be
                         // defined in the config.

  createPulseShapeFunction(cfg);

  kernelSize = round(kernelSize);

  kernel = new double[kernelSize];
  double mag = 0.0;
  for (int i = 0; i < kernelSize; i++) {
    int index = i;
    kernel[index] = pulseShape->Eval((i)*bin_size);
    mag += kernel[index];
  }

  for (int i = 0; i < kernelSize; i++) {
    kernel[i] /= mag;
  }
}
