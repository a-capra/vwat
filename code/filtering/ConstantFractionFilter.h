/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CONST_FRACTION_FILTER_STRATEGY_H
#define __CONST_FRACTION_FILTER_STRATEGY_H

#include "../data_structures/Channel.h"
#include "ConvolutionFilterStrategy.h"
#include <jsoncpp/json/json.h>

/*
 * A simple pulse baseline extraction strategy. Each waveform has it's RMS
 * calculated, the final baseline value is given in term of the waveforms mode.
 */
class ConstantFractionFilter : public IFilterStrategy {
public:
  ConstantFractionFilter();
  void filter(Channel &channel, Json::Value cfg);
  std::vector<std::string> checkParams(Json::Value params);
};

#endif // __CONST_FRACTION_FILTER_STRATEGY_H
