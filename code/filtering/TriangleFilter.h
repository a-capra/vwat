/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TRIANGLEFILTERSTRATEGY_H
#define __TRIANGLEFILTERSTRATEGY_H

#include "../data_structures/Channel.h"
#include "ConvolutionFilterStrategy.h"
#include <jsoncpp/json/json.h>

/*
 * A simple filtering triangular that uses a triangular kernel
 */
class TriangleFilter : public ConvolutionFilterStrategy {
public:
  TriangleFilter(Json::Value cfg);
};

#endif // __TRIANGLEFILTERSTRATEGY_H
