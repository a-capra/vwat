/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ConvolutionFilterStrategy.h"
#include "TROOT.h"

/**
 * ConvolutionFilterStrategy constructor, sets the kernelSize from the config
 * file
 */
ConvolutionFilterStrategy::ConvolutionFilterStrategy(Json::Value cfg) {
  kernelSize = cfg["KernelSize"].asInt();
}

/**
 * Compution the convolution of the kernel (assumed pre time reverased) and the
 * waveform stored in the `channel` The end ranges are left alone.
 */
void ConvolutionFilterStrategy::filter(Channel &channel, Json::Value cfg) {

  Waveform *wf = channel.getWaveform();
  int nBins = wf->GetNbinsX();
  Waveform *wf_out =
      new Waveform(nBins, wf->GetXaxis()->GetXmin(), wf->GetXaxis()->GetXmax());
  int kernel_offset = (kernelSize - 1) / 2;

  wf_out->SetMaximum(wf->GetMaximum());
  wf_out->SetMinimum(wf->GetMinimum());

  // endpoints ingored
  for (int i = 0; i < kernel_offset; i++) {
    wf_out->SetBinContent(i, wf->GetBinContent(i));
    wf_out->SetBinContent(nBins - i - 1, wf->GetBinContent(nBins - i - 1));
  }

  // simple convolution
  for (int i = kernel_offset; i < nBins - kernel_offset; i++) {
    double val = 0;
    double c = 0.0;
    for (int k = 0; k < kernelSize; k++) {
      val += kernel[k] * (wf->GetBinContent(i + k - kernel_offset));
    }
    wf_out->SetBinContent(i, val);
  }

  for (int i = 0; i < nBins; i++) {
    wf->SetBinContent(i, wf_out->GetBinContent(i));
  }

  wf_out->Delete();
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the algorithm depends on.
 */
std::vector<std::string>
ConvolutionFilterStrategy::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {"KernelSize"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
