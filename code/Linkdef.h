#ifdef __CLING__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ class Baseline;
#pragma link C++ class FitPulse;
#pragma link C++ class Pulse;
#pragma link C++ class Waveform;
#pragma link C++ class TTreeBranch + ;

#endif
