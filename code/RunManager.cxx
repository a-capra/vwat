/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "RunManager.h"
#include "baseline_processing/BaselineProcessor.h"
#include "event_analysis/EventEfficiency.h"
#include "event_analysis/LoLXEventProcessor.h"
#include "filtering/FilterProcessor.h"
#include "pulse_analysis/PulseAnalysisProcessor.h"
#include "pulse_finding/PulseFindingProcessor.h"
#include "reader/lngs_wav/LngsWavFactory.h"
#include "reader/proto0_laser/Proto0LaserFactory.h"
#include "reader/synthesizer/ExpSynthesizerFactory.h"
#include "reader/v1730/V1730Factory.h"
#include "reader/v1740/V1740Factory.h"
#include "writer/PulseWriteProcessor.h"
#include <cstdlib>
#include <iostream>

/**
 * Creates a Reader that acts as an Event source for further processing.
 *
 * If you add support for new device types (refer to DataFile), be sure to
 * register them in this method along with a name that can be set in the JSON
 * config.
 *
 * Initializes the shared pointer for the `event` variable. Some readers such as
 * V1740 require a specific event type (LoLXEvent).
 *
 * Returns an iterator that yields Event objects one by one from the underlying
 * data source.
 */
std::unique_ptr<Reader>
RunManager::createReader(std::shared_ptr<Event> &event) {
  std::unique_ptr<DataFileFactory> factory;
  if (dataSource.device == "V1730") {
    event = std::make_shared<Event>();
    factory = std::make_unique<V1730Factory>();
  } else if (dataSource.device == "V1740") {
    event = std::make_shared<LoLXEvent>();
    factory = std::make_unique<V1740Factory>();
  } else if (dataSource.device == "LNGS-WAV") {
    event = std::make_shared<Event>();
    factory = std::make_unique<LngsWavFactory>();
  } else if (dataSource.device == "PROTO0-LASER") {
    event = std::make_shared<Event>();
    factory = std::make_unique<Proto0LaserFactory>();
  } else if (dataSource.device == "EXP-SYNTH") {
    event = std::make_shared<Event>();
    factory = std::make_unique<ExpSynthesizerFactory>();
  } else {
    throw std::runtime_error("Device not supported");
  }

  DataFile *dataFile = factory->createDataFile(dataSource, event);
  if (dataFile->yLimitsKnown()) {
    for (int i = 0; i < channelConfigs.size(); i++) {
      channelConfigs[i].vars["NumBins"] = dataFile->getNumBins();
      channelConfigs[i].vars["MaxAmplitude"] = dataFile->getAmpMax();
      channelConfigs[i].vars["MinAmplitude"] = dataFile->getAmpMin();
    }
  }
  return dataFile->createIterator();
}

/**
 * Creates an EventLevelProcessor that acts as a final Event processing.
 *
 * If the EventLevelProcessor requires a specific event type, cast the shared
 * pointer to the correct type before creating the processor.
 */
std::unique_ptr<IEventLevelProcessor>
RunManager::createEventLevelProcessor(std::shared_ptr<Event> &event) {
  if (channelConfigs[0].eventStrategy == "LOLX") {
    std::shared_ptr<LoLXEvent> lolxEvent =
        std::dynamic_pointer_cast<LoLXEvent>(event);
    if (lolxEvent) {
      return std::make_unique<LoLXEventProcessor>(lolxEvent);
    } else {
      throw std::runtime_error("Cannot use LOLX event processor for this "
                               "Event. Must use LoLXEvent (V1740).");
    }
  } else if (channelConfigs[0].eventStrategy == "Efficiency") {
    return std::make_unique<EventEfficiency>(channelConfigs, event);
  } else if (channelConfigs[0].eventStrategy == "NA") {
    return nullptr;
  } else {
    throw std::runtime_error("Event processor not supported");
  }
}

/**
 * Initializes Reader and Event processors, then processes events until either
 * the data source is exhausted or the specified limit is reached.
 */
void RunManager::processEvents() {
  std::shared_ptr<Event> event;
  std::unique_ptr<Reader> it = createReader(event);
  FilterProcessor flProcessor{channelConfigs, event};
  BaselineProcessor bsProcessor{channelConfigs, event};
  PulseFindingProcessor pfProcessor{channelConfigs, event};
  PulseAnalysisProcessor paProcessor{channelConfigs, event};
  std::unique_ptr<IEventLevelProcessor> elProcessor =
      createEventLevelProcessor(event);
  PulseWriteProcessor wrProcessor{channelConfigs, event, dataSource};

  int c = 0, pulses = 0, totalPulses = 0, pulsesAnalyzed = 0, pulsesSaved = 0;
  while (!(it->isDone()) && c < eventLimit) {
    std::cout << "\rprocessing event " << ++c << "/" << eventLimit;

    flProcessor.processEvent();
    bsProcessor.processEvent();

    pulses = pfProcessor.processEvent();
    totalPulses += pulses;

    int pulsesSkipped = paProcessor.processEvent();
    if (pulses - pulsesSkipped > 0) {
      // event was not skipped

      if (elProcessor != nullptr) {
        elProcessor->processEvent();
      }

      pulsesSaved += wrProcessor.processEvent();
      pulsesAnalyzed += pulses - pulsesSkipped;
    }
    it->next();
  }

  if (elProcessor != nullptr) {
    elProcessor->complete();
  }

  wrProcessor.write();

  std::cout << "\n\n"
            << totalPulses << " pulses found (pre-analysis)\n\n"
            << pulsesAnalyzed << " pulses analyzed\n\n"
            << pulsesSaved << " pulses saved" << std::endl;
}