/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "RunManager.h"

/**
 * Main method for the application. Creates the RunManager singleton object and
 * kicks off the event processing. Major exceptions resulting from bad
 * configs/data/etc. are caught here.
 */
int main(int argc, char **argv) {
  if (argc != 2) {
    std::cerr << "Usage: vanwftk <path to experiment config file>" << std::endl;
    return EXIT_FAILURE;
  }

  std::unique_ptr<RunManager> rm;
  try {
    RunManager::fromConfigFile(rm, argv[1]);
  } catch (Json::RuntimeError err) {
    std::cerr << "Config file must be valid JSON:\n" << err.what() << std::endl;
    return EXIT_FAILURE;
  } catch (std::runtime_error err) {
    std::cerr << "Invalid program configuration:\n" << err.what() << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "Starting waveform analysis: [" << rm->dataSource.device << ", "
            << rm->channelConfigs.size() << " channel(s), ≤" << rm->eventLimit
            << " events]" << std::endl;

  try {
    rm->processEvents();
  } catch (std::runtime_error err) {
    std::cerr << "Could not process events:\n" << err.what() << std::endl;
  }

  std::cout << std::endl;

  return EXIT_SUCCESS;
}
