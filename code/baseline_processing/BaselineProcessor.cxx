/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "BaselineProcessor.h"
#include "MostProbBaselineStrategy.h"

BaselineProcessor::BaselineProcessor(std::vector<ChannelConfig> channelConfigs,
                                     std::shared_ptr<Event> event)
    : mEvent(event) {
  for (auto channelCfg : channelConfigs) {
    std::unique_ptr<IBaselineStrategy> bs;
    auto wsName = channelCfg.baselineStrategy;

    if (wsName == "MostProbable") {
      bs = std::make_unique<MostProbBaselineStrategy>();
    } else {
      throw std::runtime_error("Baseline strategy not recognized");
    }

    std::vector<std::string> missingParams = bs->checkParams(channelCfg.vars);
    if (!missingParams.empty()) {
      std::string missingParamsStr = "";
      for (auto param : missingParams) {
        missingParamsStr.append("\n" + param);
      }
      throw std::runtime_error("The following variables necessary for baseline "
                               "extraction are unset:" +
                               missingParamsStr);
    }

    strategies.push_back(std::move(bs));
    vars.push_back(channelCfg.vars);
  }
}

/**
 * Processes Event objects to extract basline.
 *
 * Iterates over the Event's channels and the data read in from the
 * ChannelConfigs simultaneously to pass the right context along with each
 * Waveform.
 *
 * Returns is not implemented.
 */
int BaselineProcessor::processEvent() {
  int pulseCounter = 0;
  auto channels = mEvent->getChannels();

  for (int i = 0; i < channels->size(); i++) {
    Channel channel = channels->at(i);
    strategies[i]->process(channel, vars[i]);
  }

  return pulseCounter;
}