/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PULSEFINDINGPROCESSOR_H_
#define __PULSEFINDINGPROCESSOR_H_

#include "../EventProcessor.h"
#include "../data_structures/Event.h"
#include "PulseFindingStrategy.h"

/**
 * An IEventProcessor implementation used to find and extract Pulse objects from
 * an Event's waveform data.
 *
 * This class is the RunManager's main interface with the #Pulse Finding
 * subsystem, so it's responsible for parsing the details of a ChannelConfig
 * (read and created by the RunManager) and for running the actual #pulse
 * finding code
 */
class PulseFindingProcessor : public IEventProcessor {
public:
  PulseFindingProcessor(std::vector<ChannelConfig> channelConfigs,
                        std::shared_ptr<Event> event);
  PulseFindingProcessor(){};
  int processEvent() override;

private:
  std::vector<std::unique_ptr<IPulseFindingStrategy>>
      strategies; /*!< Used internally to map strategies to their respective
                     channels. */
  std::vector<Json::Value>
      vars; /*!< Used internally to map variables from the experiment config
               file to their respective channels */
  std::shared_ptr<Event> mEvent;
};
#endif // __PULSEFINDINGPROCESSOR_H_
