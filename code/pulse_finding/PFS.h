/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PFS_H_
#define __PFS_H_

#include "PulseFindingStrategy.h"

/**
 * IPulseFindingStrategy implementation based on Giacomo's code
 * in the original WaveformProcessor.
 *
 * TODO: link to some documentation about the algorithm itself, or explain the
 * methodology at a high level -- this is out of our scope of knowledge
 */
class PFS : public IPulseFindingStrategy {
public:
  // TODO: Can we add the const back into the waveform param
  std::vector<Pulse> findPulses(Waveform &wf, Json::Value cfg) override;
  std::vector<std::string> checkParams(Json::Value params) override;
};

#endif // __PFS_H_
