/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PFSProminence.h"
#include "TROOT.h"
#include <cmath>
#include <string>

/**
 * Identifies Pulses in the Waveform and returns them in a vector.
 *
 * Pulses are identified by significant deviation in the waveform off the the
 * noise floor defined by the `Prominence` config.
 *
 * Used `WaveformStart` and `WaveformEnd` to cut off parts of the waveform
 * (required for certain endpoint condictions during filtering)
 *
 * Note: Passed all tests and acts the same as the original 'Pulse Finding
 * Algorithm'
 */
std::vector<Pulse> PFSProminence::findPulses(Waveform &wf, Json::Value cfg) {
  std::vector<Pulse> pulses;

  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double riseTimeSigmaCoefficient = cfg["RiseTimeSigmaCoefficient"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double fallTimeTauCoefficient = cfg["FallTimeTauCoefficient"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
  double noise = cfg["Noise"].asDouble();
  int polarity = cfg["Polarity"].asInt();
  double prominence = cfg["Prominence"].asDouble();
  int waveformStart = cfg["WaveformStart"].asInt();
  int waveformEnd = cfg["WaveformEnd"].asDouble();

  double baselineMu = wf.getWaveformBaseline().mu;
  if (!baselineMu) {
    // skip noisy waveform
    // return pulses;
  }

  int numBins = wf.GetNbinsX() - waveformEnd;
  double binWidth = wf.GetBinWidth(1);

  int binRiseTime = round(riseTimeSigma * riseTimeSigmaCoefficient / binWidth);
  int binFallTime =
      round((fallTimeTau * fallTimeTauCoefficient + fallTime2Tau) / binWidth);

  for (int bin = binRiseTime + 1 + waveformStart; bin <= numBins; bin++) {
    double binAmp = wf.GetBinContent(bin);
    // baselineMu = wf.GetBinContent(bin-binRiseTime);

    if (polarity * (binAmp - baselineMu) <= (noise * prominence)) {
      // didn't meet noise threshold
      continue;
    }
    double pulseAbsAmp = binAmp;

    if (true) { // left for when the later conditions are specified
      bin--;

      Pulse pulse{wf.GetBinCenter(bin),                  // time
                  pulseAbsAmp,                           // absAmp
                  polarity * (pulseAbsAmp - baselineMu), // amp
                  baselineMu,                            // baseline
                  -1,                                    // charge
                  binFallTime + binRiseTime,             // width
                  -1,
                  FitPulse()};
      pulse.fit.lowLimit = (bin - binRiseTime) * wf.GetBinWidth(1);
      pulse.fit.highLimit = (bin + binFallTime) * wf.GetBinWidth(1);
      pulses.push_back(pulse);
    }

    // move bin index to the end of the pulse
    bin += binFallTime;
  }

  return pulses;
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the algorithm depends on.
 */
std::vector<std::string> PFSProminence::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {"Noise",
                                             "RiseTimeSigma",
                                             "FallTimeTau",
                                             "FallTime2Tau",
                                             "Polarity",
                                             "RiseTimeSigmaCoefficient",
                                             "FallTimeTauCoefficient",
                                             "Prominence",
                                             "WaveformStart",
                                             "WaveformEnd"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}
