/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __LARGESTVALUE_H_
#define __LARGESTVALUE_H_

#include "PulseFindingStrategy.h"

/**
 * IPulseFindingStrategy implementation a simple pulse find strategy that sets
 * the largest value as the pulse This is intended to be used when proptyping
 * fitting algorthims on datasets which only have one (and pilled up) pulse. The
 * advantage is it much faster, but should probably not be used for proper
 * analysis
 */
class LargestValue : public IPulseFindingStrategy {
public:
  // TODO: Can we add the const back into the waveform param
  std::vector<Pulse> findPulses(Waveform &wf, Json::Value cfg) {
    std::vector<Pulse> pulses;
    int polarity = cfg["Polarity"].asInt();
    int bin;
    if (polarity < 0)
      bin = wf.GetMinimumBin();
    else
      bin = wf.GetMaximumBin();
    double pulseBaseline = wf.getWaveformBaseline().mu;

    Pulse pulse{wf.GetBinCenter(bin),                               // time
                wf.GetBinContent(bin),                              // absAmp
                polarity * (wf.GetBinContent(bin) - pulseBaseline), // amp
                pulseBaseline,                                      // baseline
                0,                                                  // charge
                0,                                                  // width
                -1,
                FitPulse()};
    pulses.push_back(pulse);
    return pulses;
  };

  std::vector<std::string> checkParams(Json::Value params) {
    std::vector<std::string> requiredParams = {"Polarity"};
    std::vector<std::string> missingParams;
    for (auto param : requiredParams) {
      if (!params.isMember(param)) {
        missingParams.push_back(param);
      }
    }
    return missingParams;
  }
};

#endif // __LARGESTVALUE_H_
