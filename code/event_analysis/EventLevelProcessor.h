/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENT_LEVEL_PROCESSOR_H
#define EVENT_LEVEL_PROCESSOR_H

#include "../EventProcessor.h"
#include <jsoncpp/json/json.h>
#include <vector>

class IEventLevelProcessor : public IEventProcessor {
public:
  virtual void complete(){};
  virtual ~IEventLevelProcessor() {}

protected:
  std::vector<Json::Value> vars;
};

#endif // EVENT_LEVEL_PROCESSOR_H