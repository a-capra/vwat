/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __EFFICIENCYEVENTPROCESSOR_H_
#define __EFFICIENCYEVENTPROCESSOR_H_

#include "EventLevelProcessor.h"

/**
 * Used To calculate the event efficiency and purity pulse finding pipeline
 * "efficiency" metric is calculated as the number of times a pulse is not found
 * in a given window "purity" metric is calculated as the number of pulses found
 * before the pulse window (counting 'false' pulses) Output the counts as a
 * faction of the number of events. Excludes channels marked as 'trigger'
 */
class EventEfficiency : public IEventLevelProcessor {
public:
  EventEfficiency(std::vector<ChannelConfig> &channelConfigs,
                  std::shared_ptr<Event> event);
  int processEvent() override;
  void complete() override;

private:
  int num_events;
  int numEffPulsesFound;
  int numFalPulsesFound;
  std::shared_ptr<Event> mEvent;
};

#endif // __EFFICIENCYEVENTPROCESSOR_H_