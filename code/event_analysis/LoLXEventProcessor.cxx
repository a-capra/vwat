/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LoLXEventProcessor.h"

/**
 * Deafult constructor
 */
LoLXEventProcessor::LoLXEventProcessor(std::shared_ptr<LoLXEvent> event) {
  mEvent = event;
};

/*
 * The main analysis for LOLX, currently and example
 * TODO: assumes the pulses are linearly distabuted (they should be) and is
 * currenlty hard coded for lolx, should possibale a better way to do it (or
 * just add values to config)
 *
 * Currently check to see is a pulse is a mutiple of the lowest pulse highted
 * and is within a certain range of the values, simslar to a histogram and then
 * taking certain evenly spaced bin
 */
int LoLXEventProcessor::processEvent() {
  double multiplicity = 0;
  double singlePulse = 300;
  double range = 100;
  mEvent->setFirstPulseTime(kMaxInt);
  for (Channel ch : *mEvent->getChannels()) {
    if (ch.getPulses()->size() <= 0)
      continue;
    double pulseTime = ch.getPulses()->front().fit.time;
    if (pulseTime < mEvent->getFirstPulseTime())
      mEvent->setFirstPulseTime(pulseTime);
    for (Pulse p : *ch.getPulses()) {
      if (p.width > 3) {
        double absAmp = abs(p.fit.amp);
        int n = round(absAmp / singlePulse);
        if (absAmp < n * singlePulse + range &&
            absAmp > n * singlePulse - range)
          multiplicity += n;
      }
    }
  }

  mEvent->setMultiplicity(multiplicity);

  return -1;
}