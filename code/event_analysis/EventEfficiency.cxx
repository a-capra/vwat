/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "EventEfficiency.h"

/**
 * EventEfficiency constructor
 * loads the channel vars into the event processor
 */
EventEfficiency::EventEfficiency(std::vector<ChannelConfig> &channelConfigs,
                                 std::shared_ptr<Event> event)
    : mEvent(event) {

  for (auto cc = channelConfigs.begin(); cc != channelConfigs.end(); cc++)
    vars.push_back(cc->vars);

  num_events = 0;
  numEffPulsesFound = 0;
  numFalPulsesFound = 0;
}

/**
 * The main processing of the events
 * Searches all the channels in the event (excluding 'trigger' channels)
 * checks if any pulses have been found inside a given window (defined by
 * 'WindowStart' and 'WindowEnd') and before the window if there is no pulse in
 * the window the numEffPulsesFound is increased. If there are pulses found
 * before the window the numEffPulsesFound is increased. Note that the reported
 * numbers include all channels for to get channel normalized number you must
 * divide by the number of non-trigger channels
 */
int EventEfficiency::processEvent() {

  int i = 0;
  for (auto channel = mEvent->getChannels()->begin();
       channel != mEvent->getChannels()->end(); channel++) {

    auto window = std::make_unique<double[]>(2);
    window[0] = vars[i]["WindowStart"].asDouble();
    window[1] = vars[i]["WindowEnd"].asDouble();
    if (vars[i]["isTrigger"].asInt() == 1)
      continue;

    num_events++;
    int found_pulse = 0;
    for (auto pulse = channel->getPulses()->begin();
         pulse != channel->getPulses()->end(); pulse++) {
      if (pulse->time > window[0] && pulse->time < window[1]) {
        found_pulse++;
      } else if (pulse->time < window[0]) {
        numFalPulsesFound++;
      }
    }
    if (found_pulse == 0)
      numEffPulsesFound++;
    i++;
  }
}

/**
 * The output of the processor.
 * "Efficiency" (num of missed pulses) and purity (number of false pulses) are
 * normalized by the number of events but not number of channels
 */
void EventEfficiency::complete() {
  // TODO: make a better output
  std::cout << std::endl
            << "Pulse efficiency in window is "
            << ((double)numEffPulsesFound) / num_events << std::endl;
  std::cout << "Pulse purity in window is "
            << ((double)numFalPulsesFound) / num_events << std::endl;
}
