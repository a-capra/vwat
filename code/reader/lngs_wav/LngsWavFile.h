/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LNGS_WAV_FILE_H
#define LNGS_WAV_FILE_H

#include "../DataFile.h"
#include "../Reader.h"
#include <cstdint>
#include <sndfile.hh>
#include <vector>

class Waveform;

/**
 * Raw Event data from each WAV file frame.
 */
struct RawEvent {
  uint16_t headerSize; /*!< Header frame. */
  uint32_t counter;    /*!< Header frame. */
  uint32_t timeTag;    /*!< Header frame. */
  uint32_t nSamples;   /*!< Header frame. */
  uint32_t nChannels;  /*!< Header frame. */
  short int *rawData;  /*!< Data frame. */
};

/**
 * LNGS liquid nitrogen laser device implementation of a DataFile for WAV
 * files.
 */
class LngsWavFile : public DataFile {
public:
  LngsWavFile(DataSource src, std::shared_ptr<Event> event);
  int getNextEvent();
  Waveform *getWaveform(int index);
  std::unique_ptr<Reader> createIterator();

  ~LngsWavFile();

private:
  std::vector<Waveform *> waveforms; /*!< Most recent waveform from the data. */
  int numChannels;    /*!< Number of channels for the experiment. */
  int currentIndex;   /*!< Current index when using #getWaveform(int index). */
  SndfileHandle file; /*!< WAV file for reading. */
  uint32_t offset;    /*!< Current offset in the file. */
  bool swapChannels;  /*!< If the signal and trigger channels are swapped. */
  int eventId;        /*!< Current ID for the Event. */
  std::shared_ptr<Event> mEvent;

  std::vector<short int> readFileSection(SndfileHandle &file, int len);
  inline uint32_t toUint32(short int v0, short int v1);
};

#endif // LNGS_WAV_FILE_H
