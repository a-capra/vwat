/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DATA_FILE_FACTORY_H
#define DATA_FILE_FACTORY_H

#include "DataFile.h"

/**
 * An abstract Factory to create data files.
 *
 * Each device must implement its own factory class.
 *
 * The factory class allows multiple files from multiple devices to be created
 * without directly accessing the data file classes.
 */
class DataFileFactory {
public:
  /**
   * DataFileFactory destructor.
   */
  virtual ~DataFileFactory(){};

  /**
   * Create and return a data file.
   *
   * The data file returned from this method is used to create an iterator
   * (Reader) to read through the file and its events.
   *
   * Each device implementation of the factory should return a new file for the
   * specific type using `src` as the parameters to create the file.
   */
  virtual DataFile *createDataFile(DataSource src,
                                   std::shared_ptr<Event> event) const = 0;
};

#endif // DATA_FILE_FACTORY_H
