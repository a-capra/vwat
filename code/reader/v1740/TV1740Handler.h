/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TV1740HANDLER_h
#define TV1740HANDLER_h

#include "TH1.h"
#include "TMidasEvent.h"
#include <vector>
using std::vector;

class TH1F;

/**
 * Class for decoding raw midas V1740 data, copied from the LOLX midastoroot
 * project
 */
class TV1740Handler { // TODO: don't hardcode constants

public:
  /**
   * Stores raw event data for one v1740
   * Always contains 64 histograms for the 64 channels
   * `exists` set to false if the waveform doesn't exist
   * this way we're not constantly re-initializing TH1F's
   */
  class EventData {
  protected:
    vector<TH1F *> waveforms;
    vector<bool> exists;
    int event_number;

  public:
    static const int ChannelsPerGroup = 8;
    static const int GroupsPerBoard = 8;
    static const int ChannelsPerBoard = ChannelsPerGroup * GroupsPerBoard;
    EventData();
    ~EventData();

    TH1F *GetWaveform(int) const;
    void ProcessEvent(unsigned int *, int, std::string bank_name = "V1740");
    int GetEventNumber() const;
    bool IsEmpty() const;
  };

  static const int NumberOfBoards = 1;
  static const int TotalChannels = NumberOfBoards * EventData::ChannelsPerBoard;
  TV1740Handler();                    // constructor
  ~TV1740Handler();                   // destructor
  void ProcessEvent(TMidasEvent &);   // main event handler
  EventData *GetEventData(int) const; // get data for 1 board
  int GetEventNumber() const;

protected:
  vector<EventData *> event_data;
};

#endif // TV1740HANDLER_h
