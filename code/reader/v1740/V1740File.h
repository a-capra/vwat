/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef V1740_FILE_H
#define V1740_FILE_H

#include "../../data_structures/LoLXEvent.h"
#include "../DataFile.h"
#include "../Reader.h"
#include "TDataContainer.hxx"
#include "TMidasEvent.h"
#include "TMidasFile.h"

class Waveform;

/**
 * V1740 device implementation of a data file.
 */
class V1740File : public DataFile {
public:
  V1740File(DataSource src, std::shared_ptr<LoLXEvent> event);
  int getNextEvent();
  Waveform *getWaveform(int index);
  std::unique_ptr<Reader> createIterator();

  /**
   * V1740 specific Midas event IDs.
   */
  enum class eventIdValue : int {
    beginRun = 0x8000, /*!< %Event ID for the beginning of a run. */
    endRun = 0x8001    /*!< %Event ID for the end of a run. */
  };

  /**
   * V1740 specific Y scale limit values.
   */
  enum class V1740YLimits {
    numBins = 16384, /*!< Number of bins for the Histogram. */
    ampMax = 16384,  /*!< Maximum amplitude for waveforms. */
    ampMin = 0       /*!< Minimum amplitude for waveforms. */
  };

  ~V1740File();

private:
  TMidasFile *midasFile;   /*!< Midas file containing the experiment data. */
  TMidasEvent *midasEvent; /*!< Current Midas event to be read. */
  TDataContainer *dataContainer;     /*!< Current Midas data for the event. */
  std::vector<Waveform *> waveforms; /*!< Most recent waveform from the data. */
  int numChannels;  /*!< Number of channels for the experiment. */
  int currentIndex; /*!< Current index when using #getWaveform(int index). */
  int eventId;      /*!< Current ID for the Event. */
  std::shared_ptr<LoLXEvent> mEvent;
};

#endif // V1740_FILE_H
