/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TV1740Handler.h"
#include "TH1.h"
#include "TMidasEvent.h"

#include <vector>
using std::vector;
#include <iostream>
using std::cout;
using std::endl;
#include <iomanip>
using std::hex;
using std::setfill;
using std::setw;

/**
 * TV1740Handler constructor.
 * Initalized the `waveform` (TH1F) and `exists` (bool) data vectors
 */
TV1740Handler::EventData::EventData() {
  for (int iChan = 0; iChan < ChannelsPerBoard; iChan++) {
    // initialize all waveforms to null.
    // they can be created as necessary.
    waveforms.push_back(0);
    exists.push_back(false);
  }
}

/**
 * TV1740Handler deconstructor.
 * Deletes waveforms TH1F used for reading the data
 */
TV1740Handler::EventData::~EventData() {
  for (vector<TH1F *>::iterator iter = waveforms.begin();
       iter != waveforms.end(); iter++)
    delete *iter; // call delete on TH1F pointer
}

/**
 * Gets the current waveform for a given channel
 * Returns nullptr if the channel is invalid
 */
TH1F *TV1740Handler::EventData::GetWaveform(int iChan) const {
  if (iChan < 0 || iChan >= ChannelsPerBoard)
    return 0;
  if (exists[iChan])
    return waveforms[iChan];
  else
    return 0;
}

/**
 * ProcessEvent the raw midas event stored in `*pdata`
 * Takes given pointer to event packet `pdata`,
 * the size of the bank to be read `bank_name`, and the name of the bank
 * `bank_name`. If pointer is null, or bank_size <= 4 just set all exists[*] to
 * false. also set event_number.
 */
void TV1740Handler::EventData::ProcessEvent(unsigned int *pdata, int bank_size,
                                            std::string bank_name) {
  // if we got an invalid pointer, or an empty bank
  if (!pdata || bank_size < 4) {
    event_number = -1;
    for (int iChan = 0; iChan < ChannelsPerBoard; iChan++)
      exists[iChan] = false;
    return;
  }
  // if we got a bank with only a header
  if (bank_size == 4) {
    event_number = pdata[2] & 0x00FFFFFF;
    for (int iChan = 0; iChan < ChannelsPerBoard; iChan++)
      exists[iChan] = false;
    return;
  }

  int event_size, group_mask, num_groups, num_samples;

  // extract header information
  event_size = pdata[0] & 0x0FFFFFFF;
  group_mask = pdata[1] & 0x000000FF;
  event_number = pdata[2] & 0x00FFFFFF;

  num_groups = 0;
  // process group mask
  for (int iGroup = 0; iGroup < GroupsPerBoard; iGroup++) {
    // this group is included
    if ((group_mask >> iGroup) & 0x1) {
      for (int iChan = 0; iChan < ChannelsPerGroup; iChan++)
        exists[iGroup * ChannelsPerGroup + iChan] = true;
      num_groups++;
    } else {

      for (int iChan = 0; iChan < ChannelsPerGroup; iChan++) {
        exists[iGroup * ChannelsPerGroup + iChan] = false;
      }
    }
  }

  // each chunk of 9 words contains 24 12-bit samples (3 samples for each of the
  // 8 channels)
  num_samples = (event_size - 4) * 3 / (9 * num_groups);

  if (0)
    cout << "count=" << event_number << " size=" << event_size
         << " bank_size=" << bank_size << " grps=" << num_groups << " "
         << num_samples << std::endl;

  if (event_size != bank_size && 0)
    cout << "WARNING: should have event_size == bank_size " << event_size << " "
         << bank_size << endl;

  // moving pointer, always points to first word of current chunk
  unsigned int *c = pdata + 4;

  // groups stored consecutively.
  for (int iGroup = 0; iGroup < GroupsPerBoard; iGroup++) {
    if (!((group_mask >> iGroup) & 0x1)) {
      continue; // skip disabled groups
    }
    int cur_chan = iGroup * ChannelsPerGroup; // chan 0 of this group

    // initialize TH1Fs
    for (int iChan = 0; iChan < ChannelsPerGroup; iChan++) {
      if (!waveforms[cur_chan + iChan]) {
        char temps[20];
        sprintf(temps, "%s_wf_%d_%d", bank_name.c_str(), iGroup, iChan);
        waveforms[cur_chan + iChan] =
            new TH1F(temps, temps, num_samples, 0, num_samples);
      } else
        waveforms[cur_chan + iChan]->SetBinsLength(num_samples);
    }

    // loop thru data for this group
    for (int iChunk = 0; (3 * iChunk) < num_samples; iChunk++, c += 9) {

      // data is 12-bit but stored in 32 bit words, so some get wrapped around
      // each "chunk" is 9 words with 3 samples per channel (24 total samples)
      // c is a pointer to the first word of each chunk
      int cur_bin = 3 * iChunk;

      // order of precedence: << and >> first, then &, then |
      waveforms[cur_chan + 0]->SetBinContent(cur_bin + 0, (c[0] & 0xFFF));
      waveforms[cur_chan + 0]->SetBinContent(cur_bin + 1, (c[0] >> 12 & 0xFFF));
      waveforms[cur_chan + 0]->SetBinContent(
          cur_bin + 2, (c[0] >> 24 & 0x0FF) | (c[1] << 8 & 0xF00));

      waveforms[cur_chan + 1]->SetBinContent(cur_bin + 0, (c[1] >> 4 & 0xFFF));
      waveforms[cur_chan + 1]->SetBinContent(cur_bin + 1, (c[1] >> 16 & 0xFFF));
      waveforms[cur_chan + 1]->SetBinContent(
          cur_bin + 2, (c[1] >> 28 & 0x00F) | (c[2] << 4 & 0xFF0));

      waveforms[cur_chan + 2]->SetBinContent(cur_bin + 0, (c[2] >> 8 & 0xFFF));
      waveforms[cur_chan + 2]->SetBinContent(cur_bin + 1, (c[2] >> 20 & 0xFFF));
      waveforms[cur_chan + 2]->SetBinContent(cur_bin + 2, (c[3] & 0xFFF));

      waveforms[cur_chan + 3]->SetBinContent(cur_bin + 0, (c[3] >> 12 & 0xFFF));
      waveforms[cur_chan + 3]->SetBinContent(
          cur_bin + 1, (c[3] >> 24 & 0x0FF) | (c[4] << 8 & 0xF00));
      waveforms[cur_chan + 3]->SetBinContent(cur_bin + 2, (c[4] >> 4 & 0xFFF));

      waveforms[cur_chan + 4]->SetBinContent(cur_bin + 0, (c[4] >> 16 & 0xFFF));
      waveforms[cur_chan + 4]->SetBinContent(
          cur_bin + 1, (c[4] >> 28 & 0x00F) | (c[5] << 4 & 0xFF0));
      waveforms[cur_chan + 4]->SetBinContent(cur_bin + 2, (c[5] >> 8 & 0xFFF));

      waveforms[cur_chan + 5]->SetBinContent(cur_bin + 0, (c[5] >> 20 & 0xFFF));
      waveforms[cur_chan + 5]->SetBinContent(cur_bin + 1, (c[6] >> 0 & 0xFFF));
      waveforms[cur_chan + 5]->SetBinContent(cur_bin + 2, (c[6] >> 12 & 0xFFF));

      waveforms[cur_chan + 6]->SetBinContent(
          cur_bin + 0, (c[6] >> 24 & 0x0FF) | (c[7] << 8 & 0xF00));
      waveforms[cur_chan + 6]->SetBinContent(cur_bin + 1, (c[7] >> 4 & 0xFFF));
      waveforms[cur_chan + 6]->SetBinContent(cur_bin + 2, (c[7] >> 16 & 0xFFF));

      waveforms[cur_chan + 7]->SetBinContent(
          cur_bin + 0, (c[7] >> 28 & 0x00F) | (c[8] << 4 & 0xFF0));
      waveforms[cur_chan + 7]->SetBinContent(cur_bin + 1, (c[8] >> 8 & 0xFFF));
      waveforms[cur_chan + 7]->SetBinContent(cur_bin + 2, (c[8] >> 20 & 0xFFF));

    } // end of iChunk loop
  }   // end of iGroup loop
}

/**
 * Gets the number of events in the dataset
 * Returns -1 if there are no events
 */
int TV1740Handler::EventData::GetEventNumber() const { return event_number; }

// returns true if all exists[*] are false
bool TV1740Handler::EventData::IsEmpty() const {
  for (int iChan = 0; iChan < ChannelsPerBoard; iChan++)
    if (exists[iChan])
      return false;
  return true;
}

/**
 * TV1740Handler constructor
 * Initializes event_data vector
 */
TV1740Handler::TV1740Handler() {
  cout << "TV1740Handler" << endl;
  for (int i = 0; i < NumberOfBoards; i++)
    event_data.push_back(new EventData());
}

/**
 * TV1740Handler constructor
 * deletes event_data vector
 */
TV1740Handler::~TV1740Handler() {
  cout << "~TV1740Handler" << endl;
  cout << "ed lenght = " << event_data.size() << " this=" << this << endl;
  for (vector<EventData *>::iterator iter = event_data.begin();
       iter != event_data.end(); iter++)
    delete *iter; // call delete on EventData pointer
}

/**
 * Gets the event data for a signal given board `iBoard`
 */
TV1740Handler::EventData *TV1740Handler::GetEventData(int iBoard) const {
  if (iBoard < 0 || iBoard >= NumberOfBoards)
    return 0;
  else
    return event_data[iBoard];
}

/**
 * Fill waveforms on all boards, given a TMidasEvent
 */
void TV1740Handler::ProcessEvent(TMidasEvent &event) {
  char bank_name[5];
  void *ptr;

  // look for banks
  for (int iBoard = 0; iBoard < NumberOfBoards; iBoard++) {
    // bank names are 4000-4003
    sprintf(bank_name, "W4%02d", iBoard);
    ptr = 0;
    int bank_size = event.LocateBank(NULL, bank_name, &ptr);
    // call event processor from EventData object.
    // it will take care of the case where bank not found.
    // cout << bank_name << " ... size=" << bank_size << " ... " << ptr << endl;
    event_data[iBoard]->ProcessEvent((unsigned int *)ptr, bank_size);
  }
}

/**
 * Gets the number of events on the first board will a valid event in the
 * dataset Returns -1 if there are no valids events across all boards
 */
int TV1740Handler::GetEventNumber() const {
  for (int iBoard = 0; iBoard < NumberOfBoards; iBoard++) {
    int temp = event_data[iBoard]->GetEventNumber();
    if (temp >= 0)
      return temp;
  }
  return -1;
}
