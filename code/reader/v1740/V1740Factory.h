/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef V1740_FACTORY_H
#define V1740_FACTORY_H

#include "../DataFileFactory.h"
#include "V1740File.h"

/**
 * V1740 device implementation of a DataFileFactory.
 */
class V1740Factory : public DataFileFactory {

public:
  DataFile *createDataFile(DataSource src,
                           std::shared_ptr<Event> event) const override;
};

#endif // V1740_FACTORY_H
