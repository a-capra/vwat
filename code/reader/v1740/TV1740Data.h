/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TV1740Data_hxx_seen
#define TV1740Data_hxx_seen

#include <vector>

#include "TGenericData.hxx"

#include "TV1740Handler.h"

/**
 * This class stores the information for
 * a single V1740 channel, in particular the waveform.
 */
class TV1740Channel {

public:
  TV1740Channel(){};
  TV1740Channel(TH1F *h1, int chan_num);

  // Get Number of samples.
  int GetNSamples() const;

  /// Get the ADC sample for a particular bin.
  int GetADCSample(int i) const;

  // copy constructor
  TV1740Channel(const TV1740Channel &rhs) {
    fChannelNumber = rhs.fChannelNumber;
    fSamples = rhs.fSamples;
  }

  // assignment operator
  TV1740Channel &operator=(const TV1740Channel &rhs) {
    fChannelNumber = rhs.fChannelNumber;
    fSamples = rhs.fSamples;
    return *this;
  }

  int GetChannelNumber() const { return fChannelNumber; };

private:
  // vector of samples for this V1740.
  std::vector<int> fSamples;

  // Channel number (on board)
  int fChannelNumber;
};

/**
 * Class to store data from CAEN V1740.
 *
 * Class stores information for data from a single board (ie single MIDAS
 * bank). Data is stored as a vector of TV1740Channel's
 *
 * Implementation detail:
 *  For the short term this class is just a thin wrapper around Alex's
 * TV1740Handler; we need to figure out how to resolve this in the longer
 * term.
 */
class TV1740Data : public TGenericData {

public:
  /// Constructor
  TV1740Data(int bklen, int bktype, const char *name, void *pdata);

  ~TV1740Data();

  /// Get the number of 32-bit words in bank.
  uint32_t GetEventSize() const { return (fGlobalHeader0 & 0xffffff); };

  /// Get the channel mask; ie, the set of channels for which we
  /// have data for this event.
  uint32_t GetGroupMask() const { return (fGlobalHeader1 & 0xff); };

  /// Get event counter
  uint32_t GetEventCounter() const { return ((fGlobalHeader2)&0xffffff); };

  /// Get trigger tag
  uint32_t GetTriggerTag() const { return ((fGlobalHeader3)&0xffffffff); };

  /// Get Number of channels in this bank.
  int GetNChannels() const { return fMeasurements.size(); }

  /// Get Channel Data
  TV1740Channel GetChannelData(int i) {
    if (i >= 0 && i < (int)fMeasurements.size())
      return fMeasurements[i];

    return TV1740Channel();
  }

  void Print();

  /// Pointer to the raw handler class.
  /// Only use for debugging!!!
  TV1740Handler::EventData *GetHandler() { return fV1740Handler; }

private:
  /// The overall global headers
  uint32_t fGlobalHeader0;
  uint32_t fGlobalHeader1;
  uint32_t fGlobalHeader2;
  uint32_t fGlobalHeader3;

  /// Vector of V1740 measurements
  std::vector<TV1740Channel> fMeasurements;

  /// pointer to TV1740Handler
  TV1740Handler::EventData *fV1740Handler;
};

#endif
