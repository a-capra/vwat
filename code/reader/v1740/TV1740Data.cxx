/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TV1740Data.h"

#include <iomanip>
#include <iostream>

/**
 * TV1740Channel Constructor
 * Take a channel id and TH1F and copies its bin contents
 */
TV1740Channel::TV1740Channel(TH1F *h1, int chan_num) {

  if (h1 == 0)
    return;

  for (int i = 0; i < h1->GetNbinsX(); i++) {
    fSamples.push_back(h1->GetBinContent(i));
  }

  fChannelNumber = chan_num;
};

/**
 * Returns the value of the waveform at a given bin
 */
int TV1740Channel::GetADCSample(int i) const {
  if (i >= 0 && i < GetNSamples())
    return fSamples[i];

  // otherwise, return error value.
  return -1;
}

/**
 * Returns the number of samples in the bin
 */
int TV1740Channel::GetNSamples() const { return fSamples.size(); }

/**
 * TV1740Data constructure that inits the vars to interface with midas
 */
TV1740Data::TV1740Data(int bklen, int bktype, const char *name, void *pdata)
    : TGenericData(bklen, bktype, name, pdata) {

  fGlobalHeader0 = GetData32()[0];
  fGlobalHeader1 = GetData32()[1];
  fGlobalHeader2 = GetData32()[2];
  fGlobalHeader3 = GetData32()[3];

  int event_size = GetData32()[0] & 0x0FFFFFFF;
  // std::cout << name << " " << event_size << std::endl;
  if (event_size > bklen || event_size == 0) {
    static int failed_v1740_decode = 0;
    failed_v1740_decode++;
    if (failed_v1740_decode <= 5) {
      std::cerr << "TV1740Data::TV1740Data : "
                << "Something wrong with this V1740 bank.  \n"
                << "The first word is " << event_size
                << " which is larger than MIDAS bank size " << bklen << ".\n"
                << "Warning " << failed_v1740_decode << " of 5." << std::endl;
    }
    fV1740Handler = 0;
    return;
  }

  // Let's create a TV1740Handler::EventData class to handle decoding the bank.
  fV1740Handler = new TV1740Handler::EventData();
  fV1740Handler->ProcessEvent((unsigned int *)pdata, bklen, name);

  for (int i = 0; i < TV1740Handler::EventData::ChannelsPerBoard; i++) {
    TH1F *tmp = fV1740Handler->GetWaveform(i);
    fMeasurements.push_back(TV1740Channel(tmp, i));
  }
}

/**
 * TV1740Data deconstructor, delete the V1740Handler
 */
TV1740Data::~TV1740Data() {

  // for(int i = 0; i < fMeasurements.size(); i++){
  //  delete fMeasurements[i];
  //}
  if (fV1740Handler)
    delete fV1740Handler;
}

/**
 * Prints the relevant information about the midas datafile
 */
void TV1740Data::Print() {

  std::cout << "V1740 decoder for bank " << GetName().c_str() << std::endl;
  std::cout << "Bank size: " << GetEventSize() << std::endl;

  //  std::cout << "Channel Mask : " << GetChannelMask() << std::endl;

  std::cout << "Event counter : " << GetEventCounter() << std::endl;
  std::cout << "Trigger tag: " << GetTriggerTag() << std::endl;

  std::cout << "Number of channels with data: " << GetNChannels() << std::endl;
  for (int i = 0; i < GetNChannels(); i++) {

    TV1740Channel channelData = GetChannelData(i);
  }
}
