/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <math.h>

#include "ExpSynthesizerFile.h"

/**
 * Synthesizer, and example synthesizer that creates a waveform of of negative
 * polarity with an exp pulse at 400 bins into the waveform with a hight of 400
 * and a baseline of 800. The usedcase of this is to check that an algorithm is
 * coded correct and is intended for troubleshooting
 */
ExpSynthesizerFile::ExpSynthesizerFile(DataSource src,
                                       std::shared_ptr<Event> event)
    : numChannels(src.channels), currentIndex(-1), eventId(0),
      mFile(src.filePath.c_str()), ttreeReader("T", &mFile),
      timestamp(ttreeReader, "timestamp"),
      branchData(ttreeReader, src.vars.get("Branch", "").asCString()) {

  for (int ch = 0; ch < numChannels; ch++) {
    waveforms.push_back(new Waveform(2, 0., 2 * 2.));
  }
}

/**
 * Deletes the waveforms. Closes the TFile.
 */
ExpSynthesizerFile::~ExpSynthesizerFile() {
  for (Waveform *wf : this->waveforms)
    wf->Delete();
  mFile.Close();
}

/**
 * Update the member mEvent.
 *
 * Each event is the same fact exp pulse with no noise
 */
int ExpSynthesizerFile::getNextEvent() {

  std::vector<Channel> channels;

  for (int i = 0; i < numChannels; i++) {
    int nBins = 30000;

    waveforms[i]->SetBins(nBins, 0., nBins * 2.);

    for (int iBin = 0; iBin < nBins; iBin++) {
      double v = 800;
      double t = iBin - nBins / 2;
      double T = 400;
      if (t > 0)
        v -= 400 * exp(-t / T);
      waveforms[i]->SetBinContent(iBin, v);
    }
    Channel channel(waveforms[i], i);
    channels.push_back(channel);
  }

  double triggerTime = 5;
  mEvent->addChannels(channels);
  mEvent->setID(eventId);
  mEvent->setTriggerTime(triggerTime);
  eventId++;

  return 0;
}

/**
 * Return the same synthetic waveform
 */
Waveform *ExpSynthesizerFile::getWaveform(int index) {
  while (index > currentIndex) {
    getNextEvent();
    currentIndex++;
  }
  if (index < currentIndex)
    std::cerr << "Index too low. Cannot read backwards." << std::endl;
  return waveforms.back();
}

/**
 * Create and return an iterator.
 *
 * This allows the RunManager to read through the events in the Midas file.
 */
std::unique_ptr<Reader> ExpSynthesizerFile::createIterator() {
  return std::make_unique<Reader>(this);
}
