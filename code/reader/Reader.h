/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef READER_H
#define READER_H

#include "../data_structures/Event.h"
#include "DataFile.h"

/**
 * A reader to iterate over events in the DataFile implementations.
 *
 * The Reader allows the RunManager to iterate over the events in a data file
 * and get those events. It can be used with any implementation of a DataFile.
 *
 * References throughout the code and documentation to 'iterator' or 'iterating'
 * refer this this class.
 */
class Reader {
public:
  Reader(DataFile *aFile);

  ~Reader();

  void next();

  bool isDone() const;

private:
  DataFile *file; /*!< Data file being iterated over. */
  Event *event;   /*!< Current Event from the file. */
  int endOfFile;  /*!< 0 if not the end of file. -1 if the end of file. */
};

#endif // READER_H
