/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Exp2Gaus3.h"

/*
 * Sets the pulse strategies fitting parameters
 * In this implementation the pulse rise, fall and baseline values are left
 * fitting parameters
 */
void Exp2Gaus3::setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) {
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asInt();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asInt();

  fit->ReleaseParameter(0);
  fit->SetParameter(0, p.baseline);

  fit->ReleaseParameter(1);
  fit->SetParameter(1, riseTimeSigma);
  fit->SetParLimits(1, riseTimeSigma * 0.1, riseTimeSigma * 10);
  fit->ReleaseParameter(2);
  fit->SetParameter(2, fallTimeTau);
  fit->SetParLimits(2, fallTimeTau * 0.1, fallTimeTau * 10);

  if (time2Frac != 0.) {

    fit->ReleaseParameter(3);
    fit->SetParameter(3, time2Frac);
    fit->SetParLimits(3, 0., 1.);
    fit->ReleaseParameter(4);
    fit->SetParameter(4, fallTimeTau);
    fit->SetParLimits(4, fallTimeTau * 0.1, fallTimeTau * 10);

  } else {

    fit->FixParameter(3, 0.);
    fit->FixParameter(4, 0.);
  }
}