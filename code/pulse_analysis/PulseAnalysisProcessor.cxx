/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PulseAnalysisProcessor.h"
#include "CR_RC.h"
#include "Exp2Gaus1.h"
#include "Exp2Gaus2.h"
#include "Exp2Gaus3.h"
#include "ExpRise.h"

/**
 * PulseAnalysisProcessor constructor.
 *
 * Takes ChannelConfigs (one per channel) and maps their contents to pair of
 * vectors. When adding new a #Pulse Analysis strategy class, register it in
 * this method along with a name that can be used in the JSON config file to
 * select it.
 */
PulseAnalysisProcessor::PulseAnalysisProcessor(
    std::vector<ChannelConfig> channelConfigs, std::shared_ptr<Event> event)
    : mEvent(event) {

  for (auto channelCfg : channelConfigs) {
    std::unique_ptr<IPulseAnalysisStrategy> pas;
    auto pasName = channelCfg.pulseAnalysisStrategy;

    if (pasName == "AdditiveChi2RefitExp2Gaus1") {
      pas = std::make_unique<Exp2Gaus1>();
    } else if (pasName == "AdditiveChi2RefitExp2Gaus2") {
      pas = std::make_unique<Exp2Gaus2>();
    } else if (pasName == "AdditiveChi2RefitExp2Gaus3") {
      pas = std::make_unique<Exp2Gaus3>();
    } else if (pasName == "AdditiveChi2RefitExpRise") {
      pas = std::make_unique<ExpRise>();
    } else if (pasName == "AdditiveChi2RefitExpRiseFloating") {
      pas = std::make_unique<ExpRiseFloating>();
    } else if (pasName == "AdditiveChi2RefitCR_RC") {
      pas = std::make_unique<CR_RC>();
    } else {
      throw std::runtime_error("Pulse analysis strategy not recognized");
    }

    std::vector<std::string> missingParams = pas->checkParams(channelCfg.vars);
    if (!missingParams.empty()) {
      std::string missingParamsStr = "";
      for (auto param : missingParams) {
        missingParamsStr.append("\n" + param);
      }
      throw std::runtime_error(
          "The following variables necessary for pulse analysis are unset:" +
          missingParamsStr);
    }

    strategies.push_back(std::move(pas));
    vars.push_back(channelCfg.vars);
  }
}

/**
 * Processes Event objects to analyze pulses.
 *
 * Iterates over the Event's channels and the data read in from the
 * ChannelConfigs simultaneously to pass the right context along with each
 * Channel being analyzed.
 *
 * Returns the total number of channels that got skipped during analysis. The
 * caller (RunManager::processEvents()) uses this to determine how many Pulses
 * were actually analyzed.
 */
int PulseAnalysisProcessor::processEvent() {

  int skipped = 0;
  auto channels = mEvent->getChannels();

  for (int i = 0; i < channels->size(); i++) {
    int doFit =
        vars[i]["doFit"].asInt(); // allows disables fit for a given channel
    auto channel = channels->at(i);
    if (doFit && strategies[i]->analyze(channels->at(i), vars[i])) {
      skipped++;
    }
  }

  return skipped;
}

/*
 * Sets the parameters of a fitting function according to the specified channel
 */
TF1 *PulseAnalysisProcessor::getFitFunction(Pulse &p, int chID) {
  TF1 *fit = strategies[chID]->createFitFunction(vars[chID], chID);
  strategies[chID]->setFitParemeters(fit, p, vars[chID]);
  return fit;
}

/**
 * Used to prepare the fit function from plotting in the event viewer and in
 * root.
 */
void PulseAnalysisProcessor::setFitFunction(std::vector<Pulse> *pulses,
                                            Waveform *wf, TF1 *fitFunction,
                                            int chID) {

  int maxNumPulsesFit = 15; // TODO: this should be from the config instead
  int tNFitPulse;
  if (pulses->size() > maxNumPulsesFit) {
    std::cout << "WARNING, number of pulses exceed max allowed for function "
              << pulses->size() << " vs " << maxNumPulsesFit << " allowed"
              << std::endl;
    tNFitPulse = maxNumPulsesFit;
  } else {
    tNFitPulse = pulses->size();
  }

  fitFunction->ReleaseParameter(5);
  fitFunction->FixParameter(5, tNFitPulse);
  // include all pulses and full range of the waveform
  fitFunction->SetLineColor(2);
  fitFunction->SetRange(wf->GetXaxis()->GetXmin(), wf->GetXaxis()->GetXmax());
  for (int i = 0; i < tNFitPulse; i++) {
    strategies[chID]->packPulseRefit(&pulses->at(i), fitFunction, i,
                                     pulses->at(i).fit.lowLimit,
                                     pulses->at(i).fit.highLimit, vars[chID]);
    std::cout << "Fit Function: "
              << "PulseID: " << i << " Time: " << pulses->at(i).fit.time
              << " Amp: " << pulses->at(i).fit.amp
              << " Chi2/NDF: " << pulses->at(i).fit.chi2 / pulses->at(i).fit.NDF
              << std::endl;
  }

  for (int i = tNFitPulse; i < maxNumPulsesFit; i++) {
    strategies[chID]->packPulseRefit(nullptr, fitFunction, i, 0, 0, vars[chID]);
  }
}