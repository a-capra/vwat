/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AdditiveChi2Refit.h"
#include "../WaveformStatistic.h"
#include "TROOT.h"
#include <Math/MinimizerOptions.h>

/**
 * Analyzes the Pulses and does all the fitting
 *
 * The analyze function takes a channel and unpacks the waveform.
 * Each pulse found by the pulse finder is fit using an exponentially modified
 * gaussian Fit pulses that are closes together as one superposed function
 */
bool IAdditiveChi2Refit::analyze(Channel &channel, Json::Value cfg) {
  const double noise = cfg["Noise"].asDouble();
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  int pulseFitLowerBoundOffset = cfg["PulseFitLowerBoundOffset"].asInt();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
  int fallTimeTauCoefficient = cfg["FallTimeTauCoefficient"].asInt();
  const double riseTimeSigmaUpperLimitCoefficient =
      cfg["RiseTimeSigmaUpperLimitCoefficient"].asDouble();
  double fallTime2TauCoefficient = cfg["FallTime2TauCoefficient"].asDouble();
  int polarity = cfg["Polarity"].asInt();
  int riseTimeSigmaCoefficient = cfg["RiseTimeSigmaCoefficient"].asInt();
  double pulseFitLowerBoundOffsetCoefficient =
      cfg["PulseFitLowerBoundOffsetCoefficient"].asDouble();
  int maxNumPulsesFit = cfg["NumPulsesFit"].asInt();

  fitOption = cfg["FitOption"].asCString(); // TODO: should this be passed into
                                            // refit or is it used elsewhere?

  std::vector<PulseGroup> pulseGroups = std::vector<PulseGroup>();
  TF1 *fitFunction = createFitFunction(cfg, channel.getChID());
  Waveform *wf = channel.getWaveform();
  std::vector<Pulse> *pulses = channel.getPulses();

  // TODO: check if the should just be removed, or left in as legacy
  if (cfg["TemplateCheck"].asInt())
    calcSinglePulseTemplateChi2(&channel, cfg);

  // sets each bins error (the 'SetError' for all bin gave a seg fault)
  for (int bin = 1; bin <= wf->GetNbinsX(); bin++)
    wf->SetBinError(bin, noise);

  int pulseGroupSize = 1; // total number of pulses fitted together

  auto pulse = pulses->begin();
  int pulseIndex =
      0; // pointers would be most robust but that requires a larger change
  while (pulse != pulses->end()) { // check if the next thing works

    double fitLowLimit = std::max(
        pulse->time - riseTimeSigma * pulseFitLowerBoundOffsetCoefficient -
            pulseFitLowerBoundOffset * wf->GetBinWidth(1),
        wf->GetXaxis()->GetXmin()); // should the 10 be a parameter?
    double fitUppLimit =
        pulse->time + fallTimeTau * fallTimeTauCoefficient +
        fallTime2Tau * fallTime2TauCoefficient +
        riseTimeSigma * riseTimeSigmaUpperLimitCoefficient +
        riseTimeSigmaUpperLimitCoefficient * wf->GetBinWidth(1);

    pulseGroupSize = 1;

    for (auto it = std::next(pulse); it != pulses->end();
         ++it) { // finding all valid pulses for the fit group

      if (fitUppLimit < it->time)
        break;

      fitUppLimit = it->time + fallTimeTau * fallTimeTauCoefficient +
                    fallTime2Tau * fallTime2TauCoefficient +
                    riseTimeSigma * riseTimeSigmaUpperLimitCoefficient +
                    riseTimeSigmaUpperLimitCoefficient * wf->GetBinWidth(1);
      pulseGroupSize++;
    }

    fitUppLimit = std::min(fitUppLimit, wf->GetBinLowEdge(wf->GetNbinsX()));

    fitFunction->FixParameter(5, pulseGroupSize);
    setFitParemeters(fitFunction, *pulse, cfg);

    for (int fitPulseIndex = 0; fitPulseIndex < pulseGroupSize;
         fitPulseIndex++) { // setting the params of the fit group to the acc
                            // pulses
      packPulseFit(&(*std::next(pulse, fitPulseIndex)), fitFunction,
                   fitPulseIndex, fitLowLimit, fitUppLimit, cfg);
    }

    // setting the unused pulses as zero
    for (int fitPulseIndex = pulseGroupSize; fitPulseIndex < maxNumPulsesFit;
         fitPulseIndex++) {
      packPulseFit(nullptr, fitFunction, fitPulseIndex, 0, 0, cfg);
    }

    fitFunction->SetRange(fitLowLimit, fitUppLimit);

    ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(100000);

    wf->Fit(fitFunction->GetName(), fitOption.c_str());

    PulseGroup group;
    group.uppLimit = fitUppLimit;
    group.lowLimit = fitLowLimit;

    for (int fitPulseIndex = 0; fitPulseIndex != pulseGroupSize;
         ++fitPulseIndex) {
      Pulse *currentPulse = &(*std::next(pulse, fitPulseIndex));
      currentPulse->fit.lowLimit = fitLowLimit;
      currentPulse->fit.highLimit = fitUppLimit;
      unpackPulseFit(currentPulse, fitFunction, fitPulseIndex);
      group.pulseGroup.push_back(pulseIndex + fitPulseIndex);
    }

    pulseGroups.push_back(group);

    pulse = std::next(pulse, pulseGroupSize);
    pulseIndex += pulseGroupSize;
  }

  if (cfg["refit"].asInt())
    refit(&channel, fitFunction, pulseGroups, cfg);

  if (pulses->size() > 1)
    sortPulses(&channel);

  channel.setValid(checkPulses(
      channel, cfg)); // sets if the chanel is valid, (polarity and pulse order)

  return channel.getValid();
}

/**
 * Returns the number of parmaters that defines the shape of the fitting
 * function
 */
int IAdditiveChi2Refit::getNumConstParams() { return 6; }

/**
 * Puts the pulse information from `pulse` into the fitting function
 * `fitFunction`, called during inital fitting of a pulse group
 */
void IAdditiveChi2Refit::packPulseFit(Pulse *pulse, TF1 *fitFunction,
                                      int fitPulseIndex, double fitLowLimit,
                                      double fitUppLimit, Json::Value cfg) {
  int basicParams = this->getNumConstParams();
  if (pulse) {
    double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
    int riseTimeSigmaCoefficient = cfg["RiseTimeSigmaCoefficient"].asInt();
    double fallTimeTau = cfg["FallTimeTau"].asDouble();
    double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
    int polarity = cfg["Polarity"].asInt();

    fitFunction->ReleaseParameter(basicParams + 2 * fitPulseIndex);
    fitFunction->SetParameter(basicParams + 2 * fitPulseIndex,
                              pulse->charge * polarity);

    if (polarity > 0)
      fitFunction->SetParLimits(basicParams + 2 * fitPulseIndex, 0, 1e10);
    else // polarity cant be zero otherwise it breaks other things
      fitFunction->SetParLimits(basicParams + 2 * fitPulseIndex, -1e10, 0);

    fitFunction->ReleaseParameter(basicParams + 1 + 2 * fitPulseIndex);
    fitFunction->SetParameter(basicParams + 1 + 2 * fitPulseIndex,
                              pulse->time -
                                  riseTimeSigma * riseTimeSigmaCoefficient);
    fitFunction->SetParLimits(basicParams + 1 + 2 * fitPulseIndex, fitLowLimit,
                              fitUppLimit);

  } else {

    fitFunction->FixParameter(basicParams + 2 * fitPulseIndex, 0);
    fitFunction->FixParameter(basicParams + 1 + 2 * fitPulseIndex, 0);
  }
}

/**
 * Puts the pulse information from `pulse` into the fitting function
 * `fitFunction`, called during refitting of a pulse group and during draw calls
 * in the EventViewer
 */
void IAdditiveChi2Refit::packPulseRefit(Pulse *pulse, TF1 *fitFunction,
                                        int fitPulseIndex, double fitLowLimit,
                                        double fitUppLimit, Json::Value cfg) {
  int basicParams = this->getNumConstParams();
  if (pulse) {
    int polarity = cfg["Polarity"].asInt();

    fitFunction->ReleaseParameter(basicParams + 2 * fitPulseIndex);
    fitFunction->SetParameter(basicParams + 2 * fitPulseIndex, pulse->fit.amp);

    if (polarity > 0)
      fitFunction->SetParLimits(basicParams + 2 * fitPulseIndex, 0, 1e10);
    else
      fitFunction->SetParLimits(basicParams + 2 * fitPulseIndex, -1e10, 0);

    fitFunction->ReleaseParameter(basicParams + 1 + 2 * fitPulseIndex);
    fitFunction->SetParameter(basicParams + 1 + 2 * fitPulseIndex,
                              pulse->fit.time);
    fitFunction->SetParLimits(basicParams + 1 + 2 * fitPulseIndex, fitLowLimit,
                              fitUppLimit);

  } else {

    fitFunction->FixParameter(basicParams + 2 * (fitPulseIndex), 0);
    fitFunction->FixParameter(basicParams + 1 + 2 * (fitPulseIndex), 0);
  }
}

/**
 * Pulls the pulse information out of the fitting fitunction `fitFunction` and
 * puts it into the `pusle`. Called during pulse fitting and refitting
 */
void IAdditiveChi2Refit::unpackPulseFit(Pulse *pulse, TF1 *fitFunction,
                                        int fitPulseIndex) {
  int basicParams = this->getNumConstParams();
  pulse->fit.amp = fitFunction->GetParameter(basicParams + 2 * fitPulseIndex);
  pulse->fit.time =
      fitFunction->GetParameter(basicParams + 1 + 2 * fitPulseIndex);

  pulse->fit.baseline = fitFunction->GetParameter(0);
  pulse->fit.riseTime = fitFunction->GetParameter(1);
  pulse->fit.fallTime = fitFunction->GetParameter(2);
  pulse->fit.time2Frac = fitFunction->GetParameter(3);
  pulse->fit.fallTime2 = fitFunction->GetParameter(4);
  pulse->fit.chi2 = fitFunction->GetChisquare();
  pulse->fit.NDF = fitFunction->GetNDF();
  pulse->fit.charge = fitFunction->Integral(
      pulse->fit.lowLimit,
      pulse->fit.highLimit); // TODO: integral is the same for each
                             // pulse in the pulse group
}

std::string IAdditiveChi2Refit::getFitFunctionName(int chID) {
  return "FExp2Gaus" + std::to_string(chID);
};

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the algorithm depends on.
 */
std::vector<std::string> IAdditiveChi2Refit::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {
      "RiseTimeSigma",
      "FallTimeTau",
      "FallTime2Tau",
      "Time2Frac",
      "FallTimeTauCoefficient",
      "RiseTimeSigmaUpperLimitCoefficient",
      "FallTime2TauCoefficient",
      "Polarity",
      "RiseTimeSigmaCoefficient",
      "NumPulsesFit",
      "TemplateCheck",
      "doFit",
      "Noise",
      "PulseFitLowerBoundOffset",
      "refit"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}

/**
 * Create the fit function for %Pulse Analysis.
 *
 * This is an example of one implementation to create a fit function that will
 * be used to analyze/fit the pulses. Different strategies may use different fit
 * functions, but the structure should be similar. A pointer to a fit function
 * is created and returned.
 *
 * This helper method should be called before the rest of the pulse analysis
 * code is executed.
 */
TF1 *IAdditiveChi2Refit::createFitFunction(Json::Value cfg, int chID) {
  TString fit_name(getFitFunctionName(chID));
  int mNFitPulseMax = cfg["NumPulsesFit"].asInt();
  TF1 *fitFunction = (TF1 *)gROOT->FindObject(fit_name);
  if (fitFunction && gROOT->FindObjectAny(fit_name)) {
    // fitFunction->Delete();
  } else {
    fitFunction =
        new TF1(fit_name, FuncExp2GausMulti, 0., 0.5e-6, 6 + mNFitPulseMax * 2);
    fitFunction->SetParLimits(1, 0., 1e-6); // sigma
    fitFunction->SetParLimits(2, 0., 1e-6); // tau
  }
  return fitFunction;
}

/**
 * Check the Pulses for order and polarity.
 *
 * Loops through all pulses in the Channel to confirm that they are properly
 * ordered and have the correct polarity. If either of these checks fail, this
 * method will return `true`, causing #analyze(Channel &channel, Json::Value
 * cfg) to return `true` and the Event to be skipped.
 *
 * Additional checks can be added as needed with the same structure -- if the
 * check fails, set `checkFailed` to true, and `break` from the loop.
 */
bool IAdditiveChi2Refit::checkPulses(Channel &channel, Json::Value cfg) {
  auto pulses = channel.getPulses();
  bool checkFailed = false;
  for (int iPulse = 0; iPulse < pulses->size(); iPulse++) {
    // CHECK IF THE PULSES ARE NON ORDERED IN TIME
    if (iPulse != 0) {
      if (pulses->at(iPulse).fit.time < pulses->at(iPulse - 1).fit.time) {
        std::cout << "\nSkipping Event: Pulses are not ordered in time."
                  << std::endl;
        checkFailed = true;
        break;
      }
    }
    // CHECK IF THE FIT PULSE POLARITY IS OK
    int polarity = cfg["Polarity"].asInt();
    if (polarity * pulses->at(iPulse).fit.amp < 0) {
      std::cout << "\nSkipping Event: Fit Pulse Polarity is wrong."
                << pulses->at(iPulse).fit.amp << std::endl;
      checkFailed = true;
      break;
    }
  }
  return checkFailed;
}

/**
 * Sorts the Pulses
 *
 * This helper method sorts the Pulses when all the Pulses have
 * completed Fitting. This method called inside analyze
 * function.
 */
void IAdditiveChi2Refit::sortPulses(Channel *channel) {

  std::vector<Pulse> *pulses = channel->getPulses();

  std::sort(pulses->begin(), pulses->end(),
            [](const Pulse &a, const Pulse &b) -> bool {
              return a.fit.time < b.fit.time;
            });
}

/**
 * Performs Refitting on the Pulses
 *
 * This helper method perform Refitting. This method
 * would is called inside analyze method.
 *
 * After a group of pulses are fit, if it's chi2 is too larger the group is
 * refit with a new new pulses added. If the chi2 has been reduced the new pulse
 * is kepted and the process is repeated. The final result is a more accurate
 * set of pulses that handles signal pile-up.
 *
 * Currently, there is a very weird convergence bug affecting ~1% of fits.
 */
bool IAdditiveChi2Refit::refit(Channel *channel, TF1 *fitFunction,
                               std::vector<PulseGroup> &pulseGroups,
                               Json::Value cfg) {
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asInt();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
  double minTimeDifference =
      cfg["MinTimeDifference"].asDouble(); // minimum time between two pulses
  int maxNumPulsesFit = cfg["NumPulsesFit"].asInt();
  int polarity = cfg["Polarity"].asInt();

  Waveform *wf = channel->getWaveform();
  std::vector<Pulse> *pulses = channel->getPulses();
  bool addedNewPulse = false;

  double minChiSquareForRefit = cfg["MinChiSquareForRefit"].asDouble();

  for (auto gp = pulseGroups.begin(); gp != pulseGroups.end(); ++gp) {

    // fit runs out of degrees of freedom
    if (pulses->at(gp->pulseGroup.front()).fit.NDF <= 0)
      continue;

    // chi2 of fit is not large enough to warrant refit
    if (pulses->at(gp->pulseGroup.front()).fit.chi2 /
            pulses->at(gp->pulseGroup.front()).fit.NDF <=
        minChiSquareForRefit)
      continue;

    fitFunction->SetRange(gp->lowLimit, gp->uppLimit);
    setFitParemeters(fitFunction, pulses->at(gp->pulseGroup.front()), cfg);
    fitFunction->ReleaseParameter(0);
    fitFunction->SetParameter(0,
                              channel->getWaveform()->getWaveformBaseline().mu);

    gp->refitChi2 = pulses->at(gp->pulseGroup.front()).fit.chi2;
    gp->refitNDF = pulses->at(gp->pulseGroup.front()).fit.NDF;

    // the main refiting loop
    while (gp->pulseGroup.size() <
           maxNumPulsesFit - 1) { // TODO: reinstate max number of pulses check?

      if (gp->refitNDF <= 0)
        break;

      if (gp->refitChi2 / gp->refitNDF <= minChiSquareForRefit)
        break;

      int fittingGroupIndex = 0;
      for (auto fittingPulse = gp->pulseGroup.begin();
           fittingPulse != gp->pulseGroup.end(); ++fittingPulse) {
        packPulseRefit(&pulses->at(*fittingPulse), fitFunction,
                       fittingGroupIndex, gp->lowLimit, gp->uppLimit, cfg);
        fittingGroupIndex++;
      }

      fitFunction->FixParameter(5, fittingGroupIndex);

      // most likely postion for new pulse (lowest chi2)
      int lastGroupBin = wf->GetXaxis()->FindBin(gp->uppLimit) - 1;
      int firstGroupBin = wf->GetXaxis()->FindBin(gp->lowLimit) + 1;
      double maxFitDifference = 0.;
      double newPulseTime = 0.;
      double newPulseAmp = 0;

      for (int bin = firstGroupBin; bin <= lastGroupBin;
           bin++) { // this gets postion where the fit has the largest positive
                    // difference (i.e where a new pulse may be)
        double fitDifference =
            polarity *
            (wf->GetBinContent(bin) - fitFunction->Eval(wf->GetBinCenter(bin)));
        if (maxFitDifference < fitDifference) {
          maxFitDifference = fitDifference;
          newPulseTime = wf->GetBinCenter(bin);
          newPulseAmp = wf->GetBinContent(bin);
        }
      }

      double currMinTimeDiff = 1.; // var for the lowest time difference between
                                   // the new pulse and the pre-existing pulses

      for (uint pulseIndex :
           gp->pulseGroup) { // maybe this should be done somehere else, like in
                             // the previous loop?
        double timeDifference =
            fabs(pulses->at(pulseIndex).fit.time - newPulseTime);
        if (currMinTimeDiff > timeDifference)
          currMinTimeDiff = timeDifference;
      }

      if (currMinTimeDiff <= minTimeDifference)
        break;

      Pulse newPulse(0, 0, 0, 0, 0, 0, -1,
                     FitPulse()); // setting up the new pulse data type

      newPulse.fit.time = newPulseTime;
      newPulse.fit.amp = newPulseAmp - fitFunction->GetParameter(0);

      packPulseRefit(&newPulse, fitFunction, fittingGroupIndex, gp->lowLimit,
                     gp->uppLimit, cfg);

      fitFunction->FixParameter(5, fittingGroupIndex + 1);

      wf->Fit(fitFunction->GetName(), fitOption.c_str());

      if (1 - fitFunction->GetChisquare() / gp->refitChi2 < 0.02)
        break;

      addedNewPulse = true;

      pushPulse(pulses, &(*gp), newPulse);

      gp->refitChi2 = fitFunction->GetChisquare();
      gp->refitNDF = fitFunction->GetNDF();

      fittingGroupIndex = 0;
      for (auto pulse = gp->pulseGroup.begin(); pulse != gp->pulseGroup.end();
           ++pulse) {

        pulses->at(*pulse).fit.lowLimit = gp->lowLimit;
        pulses->at(*pulse).fit.highLimit = gp->uppLimit;

        unpackPulseFit(&pulses->at(*pulse), fitFunction,
                       fittingGroupIndex); // TODO: this looses some
                                           // optimization from the refactor
        fittingGroupIndex++;
      }
    }
  }

  return addedNewPulse;
}

/**
 *  Performs single Pulse Chi 2
 *
 * This helper function would perform the Single Pulse Chi2 on a single pulse.
 * Sets the 'SPTemplateChi2' of each pulse in the waveform based on the fit
 * This method would be called inside analyze method.
 * Currently, it has not been tested.
 */
void IAdditiveChi2Refit::calcSinglePulseTemplateChi2(Channel *channel,
                                                     Json::Value cfg) {
  TF1 *fitFunction = createFitFunction(cfg, channel->getChID());

  double noise = cfg["Noise"].asDouble();
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
  int numPulsesFit = cfg["NumPulsesFit"].asInt();
  int riseTimeSigmaCoefficient = cfg["RiseTimeSigmaCoefficient"].asInt();
  double fallTimeTauCoefficient = cfg["FallTimeTauCoefficient"].asDouble();
  int riseTimeSigmaUpperLimitCoefficient =
      cfg["RiseTimeSigmaUpperLimitCoefficient"].asInt();

  std::vector<Pulse> *pulses = channel->getPulses();
  Waveform *wf = channel->getWaveform();

  fitFunction->SetParameter(0, wf->getWaveformBaseline().mu);
  fitFunction->SetParameter(1, riseTimeSigma);
  fitFunction->SetParameter(2, fallTimeTau);
  fitFunction->SetParameter(3, time2Frac);
  fitFunction->SetParameter(4, fallTime2Tau);
  fitFunction->SetParameter(5, 1);

  // fixing all of the other (>1) fit pulses, since we are forcing the fit of
  // only one pulse
  for (int i = 1; i < numPulsesFit; i++) {
    fitFunction->FixParameter(6 + 2 * i, 0);
    fitFunction->FixParameter(7 + 2 * i, 0);
  }

  // looping over each pulse in the waveform and doing a single fit (resuses the
  // fiting function 'fitFunction')
  for (auto pulse = pulses->begin(); pulse < pulses->end(); pulse++) {
    // setting inital conditions from the pulse
    fitFunction->SetParameter(6, pulse->charge);
    fitFunction->SetParameter(7, pulse->time -
                                     riseTimeSigma * riseTimeSigmaCoefficient);

    int firstBin =
        std::max(1, wf->GetXaxis()->FindBin(
                        pulse->time -
                        riseTimeSigma * riseTimeSigmaUpperLimitCoefficient));
    int lastBin =
        std::min(wf->GetNbinsX(),
                 int(wf->GetXaxis()->FindBin(
                         pulse->time + fallTimeTau * fallTimeTauCoefficient) +
                     fallTime2Tau)); // this part ( + 'fallTime2Tau' ) really
                                     // seams like a mistake

    double singlePulseTemplateChi2 = 0;

    for (int bin = firstBin; bin <= lastBin; bin++) {
      double delta =
          wf->GetBinContent(bin) - fitFunction->Eval(wf->GetBinCenter(bin));
      singlePulseTemplateChi2 += delta * delta / (noise * noise);
    }
    singlePulseTemplateChi2 /= (lastBin - firstBin);

    pulse->singlePulseTemplateChi2 = singlePulseTemplateChi2;
  }
}
