/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __EXPGAUS2_H_
#define __EXPGAUS2_H_

#include "AdditiveChi2Refit.h"

/**
 * An PulseFittingStrategy1 implementation used to analyze and fit Pulses in a
 * Waveform.
 *
 * Overides the PulseFittingStrategy1's 'setFitParemeters' to allow the rise and
 * fall times to be fit
 */
class Exp2Gaus2 : public IAdditiveChi2Refit {

public:
  void setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) override;
};

#endif // __EXPGAUS2_H_
