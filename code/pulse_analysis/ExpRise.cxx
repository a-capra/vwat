/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ExpRise.h"
#include "../WaveformStatistic.h"
#include "TROOT.h"

/*
 * Sets the pulse strategies fitting parameters
 * In this implementation the pulse rise, fall and baseline values are left
 * fitting parameters
 */
void ExpRise::setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) {
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asDouble();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asDouble();
  int isFloating = cfg["isFloating"].asInt();

  fit->ReleaseParameter(0);
  fit->SetParameter(0, p.baseline);

  fit->FixParameter(1, riseTimeSigma);
  fit->FixParameter(2, fallTimeTau - riseTimeSigma);
  fit->FixParameter(3, time2Frac);

  if (fallTime2Tau != 0)
    fit->FixParameter(4, fallTime2Tau - riseTimeSigma - fallTimeTau);
  else
    fit->FixParameter(4, 0);
}

std::string ExpRise::getFitFunctionName(int chID) {
  return "FExpRise" + std::to_string(chID);
};

TF1 *ExpRise::createFitFunction(Json::Value cfg, int chID) {
  TString fit_name(getFitFunctionName(chID));
  int mNFitPulseMax = cfg["NumPulsesFit"].asInt();
  TF1 *fitFunction = (TF1 *)gROOT->FindObject(fit_name);
  if (fitFunction && gROOT->FindObjectAny(fit_name)) {
    // fitFunction->Delete();
  } else {
    fitFunction =
        new TF1(fit_name, FuncExpRiseMulti, 0., 0.5e-6, 6 + mNFitPulseMax * 2);
    fitFunction->SetParLimits(1, 0., 1e-6); // sigma
    fitFunction->SetParLimits(2, 0., 1e-6); // tau
  }
  return fitFunction;
}

/*
 * Sets the pulse strategies fitting parameters
 * In this implementation the pulse rise, fall and baseline values are left
 * fitting parameters
 */
void ExpRiseFloating::setFitParemeters(TF1 *fit, Pulse &p, Json::Value cfg) {
  double riseTimeSigma = cfg["RiseTimeSigma"].asDouble();
  double fallTimeTau = cfg["FallTimeTau"].asInt();
  double time2Frac = cfg["Time2Frac"].asDouble();
  double fallTime2Tau = cfg["FallTime2Tau"].asInt();

  fit->ReleaseParameter(0);
  fit->SetParameter(0, p.baseline);

  fit->ReleaseParameter(1);
  fit->SetParameter(1, riseTimeSigma);
  fit->SetParLimits(1, riseTimeSigma * 0.1, riseTimeSigma * 10);
  fit->ReleaseParameter(2);
  fit->SetParameter(2, fallTimeTau);
  fit->SetParLimits(2, fallTimeTau * 0.1, fallTimeTau * 10);

  if (time2Frac != 0.) {

    fit->ReleaseParameter(3);
    fit->SetParameter(3, time2Frac);
    fit->SetParLimits(3, 0., 1.);
    fit->ReleaseParameter(4);
    fit->FixParameter(4, fallTimeTau);
    fit->SetParLimits(4, fallTimeTau * 0.1, fallTimeTau * 10);

  } else {
    fit->FixParameter(3, 0.);
    fit->FixParameter(4, 0.);
  }
}