/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOLX_TTREE_WRITER_H
#define LOLX_TTREE_WRITER_H

#include "../data_structures/LoLXEvent.h"
#include "TTreeWriter.h"

/**
 * A LoLX implementation of TTreeWriter.
 *
 * The LoLXTTreeWriter adds additional event level variables as branched in the
 * outputted TTree.
 */
class LoLXTTreeWriter : public TTreeWriter {
public:
  LoLXTTreeWriter(Json::Value cfg, DataSource dataSource,
                  std::shared_ptr<LoLXEvent> event);
  void pushEventVariables();

private:
  Double_t firstPulseTime; /*!< Time of the first pulse in an Event. */
  Int_t multiplicity;      /*!< Number of photons in an event */
  std::shared_ptr<LoLXEvent>
      mEvent; /*!< Overrides base TTreeWriter mEvent variable */
};

#endif // LOLX_TTREE_WRITER_H
