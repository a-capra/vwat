/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TTreeWriter.h"
#include <string>

/**
 * TTreeWriter constructor.
 * Sets up the TFile to write to and creates the branches for each Channel.
 */
TTreeWriter::TTreeWriter(Json::Value cfg, DataSource dataSource)
    : written(false), eventID(0), triggerTime(0) {

  mProcDir = gDirectory;
  std::string outputFilePath;
  if (dataSource.outFilePath.empty()) {
    outputFilePath = dataSource.filePath;
  } else {
    outputFilePath = dataSource.outFilePath;
  }

  std::string fullOutputPath = outputFilePath.append(".TTree.root");

  mOutputFile = new TFile(fullOutputPath.c_str(), "RECREATE");

  mTree = new TTree("DataOutTree", "Data Results");

  for (int channelID = 0; channelID < dataSource.channels; channelID++) {
    branches.push_back(new TTreeBranch());
    std::string suffix = "ch" + std::to_string(channelID);
    mTree->Branch((suffix).c_str(), "TTreeBranch", branches[channelID]);
  }
  mTree->Branch("eventID", &eventID, "eventID/I");
  mTree->Branch("triggerTime", &triggerTime, "triggerTime/D");

  mProcDir->cd();
}

/**
 * Writes the TTree to disk.
 */
void TTreeWriter::write() {
  if (!written) {
    written = true;
    mOutputFile->cd();
    mTree->Write();
    mOutputFile->Close();
    mProcDir->cd();
  }
}

/**
 * Updates the TTreeBranch for each Channel at each Event.
 */
bool TTreeWriter::push(int indexEvent, Channel &ch, int chID, Waveform &wf,
                       Json::Value cfg, double eventTriggerTime) {
  eventID = indexEvent;
  triggerTime = eventTriggerTime;
  bool containsPulses = branches[chID]->updateBranch(indexEvent, ch);

  // only fill the tree when the last channel branch has been updated.
  if (chID == branches.size() - 1) {
    mTree->Fill();
  }

  return containsPulses;
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the writing code depends on. There are currently no required
 * parameters for this writer.
 */
std::vector<std::string> TTreeWriter::checkParams(Json::Value params) {
  return {};
}

/**
 * TTreeWriter deconstructor.
 */
TTreeWriter::~TTreeWriter() {
  for (TTreeBranch *tbranch : this->branches)
    tbranch->Delete();
  mOutputFile->Close();
  delete mOutputFile;
}