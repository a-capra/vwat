/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __IPULSEWRITER_H
#define __IPULSEWRITER_H

#include "../data_structures/Channel.h"
#include "../data_structures/Event.h"
#include "../data_structures/Pulse.h"
#include "../data_structures/Waveform.h"
#include "jsoncpp/json/json.h"
#include <vector>

/**
 * An interface for #writing pulses to file. If you want to
 * develop a new #Pulse Writer, have it inherit from this class and
 * override the two virtual methods (refer to TTreeWriter for an example).
 * Designed to be called from PulseWriteProcessor.
 */
class IPulseWritingStrategy {
public:
  virtual void write() = 0;
  virtual bool push(int indexEvent, Channel &ch, int chID, Waveform &wf,
                    Json::Value cfg, double eventTriggerTime) = 0;
  virtual void pushEventVariables() = 0;
  virtual std::vector<std::string> checkParams(Json::Value params) = 0;
};
#endif // __IPULSEWRITER_H
