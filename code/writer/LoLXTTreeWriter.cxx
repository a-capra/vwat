/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LoLXTTreeWriter.h"

/**
 * LoLXTTreeWriter constructor.
 *
 * Calls the TTreeWriter constructor, sets the `mEvent` variable and adds
 * branches to the TTree for multipicity and first pulse time.
 */
LoLXTTreeWriter::LoLXTTreeWriter(Json::Value cfg, DataSource dataSource,
                                 std::shared_ptr<LoLXEvent> event)
    : TTreeWriter(cfg, dataSource), mEvent(event) {
  // create branches for each new event level variable
  mTree->Branch("multiplicity", &multiplicity, "multiplicity/I");
  mTree->Branch("firstPulseTime", &firstPulseTime, "firstPulseTime/D");
}

/**
 * Updates the event level branches with event specific data.
 *
 * Set the multiplicity and first pulse time from the Event.
 */
void LoLXTTreeWriter::pushEventVariables() {
  multiplicity = mEvent->getMultiplicity();
  firstPulseTime = mEvent->getFirstPulseTime();
}
