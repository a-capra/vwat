/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "NTupleWriter.h"
#include <string> // std::string

/*
 * NTupleWriter constructor
 * Sets up the TFile objects to write to.
 */
NTupleWriter::NTupleWriter(Json::Value cfg, DataSource dataSource) {

  mProcDir = std::unique_ptr<TDirectory>(gDirectory);

  std::string outputFilePath;
  if (dataSource.outFilePath.empty()) {
    outputFilePath = dataSource.filePath;
  } else {
    outputFilePath = dataSource.outFilePath;
  }

  for (int channelID = 0; channelID < dataSource.channels; channelID++) {
    char fullOutputPath[80];
    sprintf(fullOutputPath, "%s.Ch%iNTuple.root", outputFilePath.c_str(),
            channelID);
    std::string ntp_name = "ntp" + std::to_string(channelID);

    outputFiles.push_back(
        {std::make_unique<TFile>(fullOutputPath, "RECREATE"),
         std::make_unique<TNtuple>(
             ntp_name.c_str(), ntp_name.c_str(),
             "evt:tt:blmu:blRMS:np:pbl:paa:pa:pt:pq:pw:tchi2:fa:ft:frt:"
             "fft:ff2:fft2:fblmu:fchi2:ndf:frchi2:frndf:fcharge")});
  }

  mProcDir->cd();
}

/*
 * Writes the NTuples to disk
 */
void NTupleWriter::write() {

  for (int i = 0; i < outputFiles.size(); i++) {
    outputFiles[i].mOutFile->cd();
    outputFiles[i].mNtp->Write();
    outputFiles[i].mOutFile->Close();
  }
  mProcDir->cd();
}

/**
 * Pushes the events to the NTuple to then be written to file when processing
 * has completed.
 *
 * TODO: This could instead be done in batches since if something
 * crashes it would be nice if it could be restarted.
 */
bool NTupleWriter::push(int indexEvent, Channel &ch, int chID, Waveform &wf,
                        Json::Value cfg, double eventTriggerTime) {
  int number_pulse = ch.getPulses()->size();

  if (number_pulse == 0)
    return false;

  int doFit = cfg["doFit"].asInt();
  std::vector<Pulse> *pulses = ch.getPulses();
  for (auto it = pulses->begin(); it < pulses->end(); it++) {
    float ntpCont[24];
    ntpCont[0] = indexEvent;
    ntpCont[1] = eventTriggerTime;
    ntpCont[2] = wf.getWaveformBaseline().mu;
    ntpCont[3] = wf.getWaveformBaseline().RMS;
    ntpCont[4] = number_pulse;
    ntpCont[5] = it->baseline;
    ntpCont[6] = it->absAmp;
    ntpCont[7] = it->amp;
    ntpCont[8] = it->time;
    ntpCont[9] = it->charge;
    ntpCont[10] = it->width;

    if (!doFit) {
      ntpCont[11] = 0;
      ntpCont[12] = 0;
      ntpCont[13] = 0;
      ntpCont[14] = 0;
      ntpCont[15] = 0;
      ntpCont[16] = 0;
      ntpCont[17] = 0;
      ntpCont[18] = 0;
      ntpCont[19] = 0;
      ntpCont[20] = 0;
      ntpCont[21] = 0;
      ntpCont[22] = 0;
      ntpCont[23] = 0;
    } else {
      ntpCont[11] = it->singlePulseTemplateChi2;
      ntpCont[12] = it->fit.amp;
      ntpCont[13] = it->fit.time;
      ntpCont[14] = it->fit.riseTime;
      ntpCont[15] = it->fit.fallTime;
      ntpCont[16] = it->fit.time2Frac;
      ntpCont[17] = it->fit.fallTime2;
      ntpCont[18] = it->fit.baseline;
      ntpCont[19] = it->fit.chi2;
      ntpCont[20] = it->fit.NDF;
      ntpCont[21] = 0; // wfProc.getChi2Refit(iPulse);
      ntpCont[22] = 0; // wfProc.getNDFRefit(iPulse);
      ntpCont[23] = it->fit.charge;
    }

    outputFiles[chID].mNtp->Fill(ntpCont);
  }

  return true;
}

/**
 * Checks the runtime parameters and returns a vector containing any missing
 * parameters that the writing code depends on.
 */
std::vector<std::string> NTupleWriter::checkParams(Json::Value params) {
  std::vector<std::string> requiredParams = {"doFit"};
  std::vector<std::string> missingParams;
  for (auto param : requiredParams) {
    if (!params.isMember(param)) {
      missingParams.push_back(param);
    }
  }
  return missingParams;
}