/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __NTUPLEWRITER_H_
#define __NTUPLEWRITER_H_

#include "../reader/DataFile.h"
#include "PulseWritingStrategy.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TROOT.h"
#include <string>

/*
 * Holds pointers the the root objects for a given channel.
 * This removed the need to do root dictionary lookups
 */
struct ChannelOutput {
  std::unique_ptr<TFile> mOutFile;
  std::unique_ptr<TNtuple> mNtp;
};

/**
 * IPulseWritingStrategy implementation in the simple NTuple
 * in the original WaveformProcessor.
 */
class NTupleWriter : public IPulseWritingStrategy {
public:
  NTupleWriter(Json::Value cfg, DataSource dataSource);
  void write();
  bool push(int indexEvent, Channel &ch, int chID, Waveform &wf,
            Json::Value cfg, double eventTriggerTime);
  void pushEventVariables(){};
  std::vector<std::string> checkParams(Json::Value params);

private:
  std::unique_ptr<TDirectory> mProcDir;
  std::vector<ChannelOutput> outputFiles;
};

#endif // __NTUPLEWRITER_H_
