/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TTreeBranch.h"

/**
 * Updates the data for the branch to be filled.
 * Sets the number of pulses and pulses from the Channel
 * for the Event.
 */
bool TTreeBranch::updateBranch(int indexEvent, Channel &ch) {
  // empty the pulse array
  pulse->Clear();

  std::vector<Pulse> *pulses = ch.getPulses();
  int number_pulse = pulses->size();
  npulse = number_pulse;

  if (number_pulse == 0)
    return false;

  int i = 0;
  for (auto it = pulses->begin(); it < pulses->end(); it++) {
    new ((*pulse)[i]) Pulse(*it);
    i++;
  }
  return true;
}

/**
 * TTreeBranch deconstructor.
 */
TTreeBranch::~TTreeBranch() { pulse->Delete(); }