/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PULSEWRITEPROCESSOR_H_
#define __PULSEWRITEPROCESSOR_H_

#include "../EventProcessor.h"
#include "../data_structures/Event.h"
#include "LoLXTTreeWriter.h"
#include "NTupleWriter.h"
#include "TTreeWriter.h"

/**
 * An IEventProcessor implementation used write events to file
 *
 * This class is the RunManager's main interface with the #Writing
 * subsystem, so it's responsible for parsing the details of a ChannelConfig
 * (read and created by the RunManager) and for running the actual writing
 * code
 */
class PulseWriteProcessor : public IEventProcessor {
public:
  PulseWriteProcessor(std::vector<ChannelConfig> channelConfigs,
                      std::shared_ptr<Event> event, DataSource dataSource);
  int processEvent() override;
  void write();
  ~PulseWriteProcessor();

private:
  std::vector<IPulseWritingStrategy *>
      strategies; /*!< Used internally to map strategies
            to their respective channels. */
  std::vector<Json::Value>
      vars; /*!< Used internally to map variables from the experiment config
               file to their respective channels */
  bool sharedWriter; /*!< Flag for if the strategy uses a single shared writer.
                      */
  std::shared_ptr<Event> mEvent;
};
#endif // __PULSEWRITEPROCESSOR_H_
