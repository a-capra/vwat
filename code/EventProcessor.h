/*
 * Copyright (C) 2020-2021 The VWAT Contributors
 *
 * This file is part of VWAT.
 *
 * VWAT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VWAT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VWAT.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __EVENTPROCESSOR_H_
#define __EVENTPROCESSOR_H_

#include "data_structures/Event.h"
#include <jsoncpp/json/json.h>
#include <vector>

/**
 * A struct containing basic config values that indicate how a channel should be
 * processed. Implementations of IEventProcessor specify how this data is used.
 */
struct ChannelConfig {
  std::string filterStrategy;
  std::string baselineStrategy;
  std::string
      pulseFindingStrategy; /*!< The type of #Pulse Finding strategy to use. */
  std::string pulseAnalysisStrategy; /*!< The type of #Pulse Analysis strategy
                                        to use. */
  std::string eventStrategy;   /*!< The type of processor strategy to use for
                                  analysing all channels. */
  std::string writingStrategy; /*!< The type of writing strategy to use. */

  Json::Value
      vars; /*!< Map of values taken from the JSON experiment config file. It is
               up to implementations of IEventProcessor to validate this (refer
               to PFS::checkParams() for an example) */
};

/**
 * Process the data on an event by event, after pulse analysis
 */
class IEventProcessor {
public:
  virtual int processEvent() = 0;
  virtual ~IEventProcessor() {}
};

#endif // __EVENTPROCESSOR_H_
