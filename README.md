# Vancouver Waveform Analysis Toolkit

# Overview

A software suite for processing waveform data, designed to help particle physics researchers detect and analyze pulses. VWAT is open source and licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See `COPYING` and `LICENSE` files for more information.

# Environment Setup

We **strongly** recommend using [Docker](https://docs.docker.com/) as a development and execution environment for this software. Follow [our guide](docs/DOCKER.md) to get set up with Docker quickly and to understand how we use it in this project. Most of the following instructions assume that you have set Docker up on your system, and that you are running most commands here in the container environment.

If not using Docker, you can still refer to the `Dockerfile` as a reference for the steps required to set up your environment to use VWAT.

We also recommend installing [`clang-format`](https://clang.llvm.org/docs/ClangFormat.html) if you plan to contribute to this project.

## Dependencies

The project's dependencies are baked into our container image, so you should not need to worry about them if you use our run scripts (`scripts/docker-run-*.sh`) to spin up the environment.

The following dependencies must be installed and available on the system to successfully compile and run the software.

- [ROOT](https://root.cern.ch/)
- [MIDAS](https://midas.triumf.ca/MidasWiki/index.php/Main_Page)
- [ROOTANA](https://midas.triumf.ca/MidasWiki/index.php/ROOTANA)
- [`jsoncpp`](https://github.com/open-source-parsers/jsoncpp)
- [`libsndfile`](https://github.com/libsndfile/libsndfile)

Refer to the top section of the project `Makefile` for a list of environment variables that must be set for successful compilation.

# Building

Run `make` to build the project. Resulting object files will be placed under `obj/`, shared libraries under `lib/`, and executables under `bin/`. Run `make clean all` to do a clean build.

Ensure that your compiler is compatible with C++14.

# Usage

Refer to our [Docker](docs/DOCKER.md) documentation for instructions on setting up and running the project in a portable, lightweight virtual environment.

**Step 1:** Set your data sources, channel configurations, algorithm strategies, and variables in a JSON file (refer to `experiments/example.json` for an example and [Configuration File Documentation](experiments/CONFIG.md) for specific details)

## Option 1 - vanwftk

**Step 2:** Run the `bin/vanwftk` executable and pass the relative path to your JSON config file as the only command line parameter (eg. `./bin/vanwftk experiments/example.json`)

**Step 3:** Explore the output (written to `ntp/` or specified `OutputFilePath`) with ROOT

## Option 2 - EventViewer

**Step 2:** Run the `bin/EventViewer` executable and pass the relative path to your JSON config file as the only command line parameter (eg. `./bin/EventViewer experiments/example.json`)

**Step 3:** View the output event by event in the ROOT Browser

# Development

Refer to our [CONTRIBUTING.md](CONTRIBUTING.md).
