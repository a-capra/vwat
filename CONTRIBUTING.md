# Contributing

# Contributing to VWAT

This document outlines what developers need to know before contributing code to this project. The general workflow is as follows:

1. Pull the latest changes from `master` (or whatever your base branch is)
2. Check out a new feature branch
3. Commit some changes and push your branch to this GitLab repo
4. View your branch in the GitLab UI and check that the pipeline passed (you'll see a green check mark)
5. Repeat steps 3 and 4 as necessary
6. Open a merge request and ask maintainers to review your work
7. After a maintainer reviews and accepts your merge request, your new code becomes a part of the stable branch!

**Note:** Unless you are a member of the VWAT group, you will first need to fork the project to be able create and push a branch that you can then open a merge request for to merge into the `vwat` repository.
Refer to the sections below and the resources linked at the bottom of this document for instructions on how to do these steps.

# Branching Strategy

Our Git branching strategy is pretty loose in this project, but we consider `master` to be the stable branch.

Our branch naming convention is `<type>/<name>`. For example, a branch adding a new feature could be called `feature/new-feature-x`. This is not necessary, but it keeps branches organized and lets maintainers estimate how much time they'll have to spend reviewing new contributions (i.e. a `refactor/` or `feature/` branch will probably require more effort than a `docs/` branch). Following this naming convention also makes it easier to write automation scripts that process branches differently based on their name; we don't currently use this feature but it's good to keep in mind.

# Continuous Integration

[Continuous Integration](https://www.atlassian.com/continuous-delivery/continuous-integration) is a broad topic, but for our purposes it's the process that lets us automatically check and build every commit you push to our repo. By offloading this work to our pipeline (defined in `.gitlab-ci.yml`, learn more [here](https://docs.gitlab.com/ee/ci/introduction/)), we can hold every commit in every branch to a uniform standard of quality. If a branch doesn't compile or doesn't follow our coding standards, the pipeline will fail and will not build a container image, ensuring that end users will not be using broken code.

# Merge Requests

Our continuous integration pipeline ensures that only good code gets built for users, but automation can only check so much. If you're adding new features, you'll want to use [GitLab Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/) to let maintainers approve your contributions before they get added to a stable branch. You can make a Merge Request through the GitLab UI -- refer to the official documentation and our [merge history](https://gitlab.com/vwat/vwat/-/merge_requests?scope=all&state=merged) for examples of what information a merge request should contain.

# Code Style

## C++

This project integrates with [`clang-format`](https://clang.llvm.org/docs/ClangFormat.html) to ensure that the source code adheres to the [LLVM Coding Standards](https://llvm.org/docs/CodingStandards.html) for C++.

Install `clang-format` locally via Homebrew (MacOS) or your preferred package manager. Run `make lint` to detect any style errors, or just run `make format` before making a commit to let the tool automatically format your code. The CI pipeline running for every commit will reject code that does not pass the linting step, ensuring that all of the code merged to the `master` branch follows the standard style.

## Scripts

Shell scripts located under the `scripts/` directory are also linted with [Shellcheck](https://github.com/koalaman/shellcheck), which catches all kinds of mistakes that may cause unexpected issues. You may check your scripts locally using `make shellcheck` (assuming Shellcheck is installed), or via its [web client](https://www.shellcheck.net/).

# Resources

- [Learn Git](https://try.github.io/)
- [GitFlow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
- [GitLab Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/)
- [GitLab Pipelines Intro](https://docs.gitlab.com/ee/ci/introduction/)
