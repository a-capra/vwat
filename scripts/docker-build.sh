#!/bin/bash

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

PROJECT_ROOT_PATH="$(dirname "$(dirname "$(realpath "$0")")")"
IMAGE="registry.gitlab.com/vwat/vwat/image:$(git branch --show-current | sed 's/\//-/g; s/_/-/g')"

if [[ "$1" == "--rebuild" ]]; then
  docker build --no-cache -t "$IMAGE" "$PROJECT_ROOT_PATH"
else
  docker build -t "$IMAGE" "$PROJECT_ROOT_PATH"
fi
