#!/usr/bin/env sh

make lint 2> /dev/null
LINT_SUCCESS="$?"

[ "$LINT_SUCCESS" -ne 0 ] && printf "\n\nPipeline failed: Code is not formatted correctly; Run 'make format' and push again\n"

exit "$LINT_SUCCESS"
