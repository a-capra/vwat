<#
    .SYNOPSIS
        Run a docker container for the current branch.

    .EXAMPLE
        PS C:\src\vwat> ./scripts/docker-run-windows.ps1 -p C:\Documents\VWATData -d

        Using image tag 'develop'... 
        Mounting directory: 'C:\Documents\VWATData' to '/home/vanwftk/data'
        Mounting directory: 'C:\src\vwat' to '/home/vanwftk/vwat'
        root@4bb77fba2e52:/home/vanwftk/vwat#
#>
param
(
  [Parameter(Position=0)]
  [string]$p,
  [Parameter(Mandatory=$false)]
  [Switch]$d
)

$scriptName = &{ $myInvocation.ScriptName }
$projectRootPath=Split-Path -Path (Split-Path -Path $scriptName)
$containerWorkdir='/home/vanwftk/vwat'

$image="registry.gitlab.com/vwat/vwat/image"
$branchTag = ((git branch --show-current) -replace '[^a-zA-Z\d]', '-').ToLower()
$completeImage= "${image}:${branchTag}"

docker manifest inspect "${completeImage}" | Out-Null
if (-not $LASTEXITCODE) {
  Write-Host "Using image tag '$branchTag'..."
  docker pull "$completeImage"
} else {
  Write-Host "Could not find an image associated with this branch; defaulting to 'master'"
  $completeImage="${image}:master"
}

if (([string]::IsNullOrEmpty($p)) -and ($d -eq $false)) {
  $projectDirMapping="${projectRootPath}/ntp:${containerWorkdir}/ntp"
  $experimentsMapping="${projectRootPath}/experiments:${containerWorkdir}/experiments"

  Write-Host "No directories specified; mounting default directories
              Mounting directory: '${projectRootPath}/ntp' to '${containerWorkdir}/ntp'
              Mounting directory: '${projectRootPath}/experiments' to '${containerWorkdir}/experiments'"

  docker run --rm -it -v $projectDirMapping -v $experimentsMapping `
  -e DISPLAY=host.docker.internal:0 `
  $completeImage
} elseif ((-not ([string]::IsNullOrEmpty($p))) -and (-not ($d -eq $false))) {
  $projectDataMapping="${p}:/home/vanwftk/data"
  $projectDirMapping="${projectRootPath}:${containerWorkdir}"

  Write-Host "Mounting directory: '${p}' to '/home/vanwftk/data'"
  Write-Host "Mounting directory: '${projectRootPath}' to '${containerWorkdir}'"

  docker run --rm -it -v $projectDataMapping -v $projectDirMapping `
  -e DISPLAY=host.docker.internal:0 `
  $completeImage
} elseif ((-not ([string]::IsNullOrEmpty($p))) -and ($d -eq $false)) {
  $projectDataMapping="${p}:/home/vanwftk/data"
  $experimentsMapping="${projectRootPath}/experiments:${containerWorkdir}/experiments"

  Write-Host "Mounting directory: '${p}' to '/home/vanwftk/data'"
  Write-Host "Mounting directory: '${projectRootPath}/experiments' to '${containerWorkdir}/experiments'"

  docker run --rm -it -v $projectDataMapping -v $experimentsMapping `
  -e DISPLAY=host.docker.internal:0 `
  $completeImage
} elseif (([string]::IsNullOrEmpty($p)) -and (-not ($d -eq $false))) {
  $projectDirMapping="${projectRootPath}:${containerWorkdir}"

  Write-Host "Mounting directory: '${projectRootPath}' to '${containerWorkdir}'"

  docker run --rm -it -v $projectDirMapping `
  -e DISPLAY=host.docker.internal:0 `
  $completeImage 
}