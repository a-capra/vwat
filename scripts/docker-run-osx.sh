#!/bin/bash

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

PROJECT_ROOT_PATH="$(dirname "$(dirname "$(realpath "$0")")")"
CONTAINER_WORKDIR='/home/vanwftk/vwat'

ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + "$ip"

while getopts ":p:d" flag; do
  case ${flag} in
    p )
      if [[ "$OPTARG" == -* ]]; then
        printf "usage: ./docker-run-osx.sh [-p datapath] [-d]
                -p datapath: local path to experiment data
                -d:          use develop mode (mount code directory)\n"
        exit 1
      fi
      LOCAL_DATA_PATH="$OPTARG"
      PROJECT_DATA_MAPPING="$OPTARG:/home/vanwftk/data"
      ;;
    d )
      PROJECT_DIR_MAPPING="${PROJECT_ROOT_PATH}/:${CONTAINER_WORKDIR}/"
      ;;
    : )
      printf "usage: ./docker-run-osx.sh [-p datapath] [-d]
              -p datapath: local path to experiment data
              -d:          use develop mode (mount code directory)\n"
      exit 1
      ;;
    * )
      printf "usage: ./docker-run-osx.sh [-p datapath] [-d]
              -p datapath: local path to experiment data
              -d:          use develop mode (mount code directory)\n"
      exit 1
      ;;
  esac
done

[ -n "$PROJECT_DATA_MAPPING" ] && [ -z "$PROJECT_DIR_MAPPING" ] && EXPERIMENTS_MAPPING="${PROJECT_ROOT_PATH}/experiments:${CONTAINER_WORKDIR}/experiments"

[ -n "$PROJECT_DATA_MAPPING" ] && echo "Mounting directory: '$LOCAL_DATA_PATH' to '/home/vanwftk/data'"
[ -n "$EXPERIMENTS_MAPPING" ] && echo "Mounting directory: '${PROJECT_ROOT_PATH}/experiments' to '${CONTAINER_WORKDIR}/experiments'"
[ -n "$PROJECT_DIR_MAPPING" ] && echo "Mounting directory: '${PROJECT_ROOT_PATH}' to '${CONTAINER_WORKDIR}'"

if [[ "$OPTIND" == 1 ]]; then
  PROJECT_DIR_MAPPING="${PROJECT_ROOT_PATH}/ntp:${CONTAINER_WORKDIR}/ntp"
  EXPERIMENTS_MAPPING="${PROJECT_ROOT_PATH}/experiments:${CONTAINER_WORKDIR}/experiments"
  printf "No directories specified; mounting default directories
          Mounting directory: '%s/ntp' to '%s/ntp'
          Mounting directory: '%s/experiments' to '%s/experiments'\n" "${PROJECT_ROOT_PATH}" "${CONTAINER_WORKDIR}" "${PROJECT_ROOT_PATH}" "${CONTAINER_WORKDIR}"
fi

IMAGE="registry.gitlab.com/vwat/vwat/image"
TAG="$(git branch --show-current | sed 's/\//-/g; s/_/-/g')"

if DOCKER_CLI_EXPERIMENTAL=enabled docker manifest inspect "${IMAGE}:${TAG}" > /dev/null; then
  echo "Using image tag '$TAG'..."
  docker pull "${IMAGE}:${TAG}"
else
  echo "Could not find an image associated with this branch; defaulting to 'master'"
  TAG='master'
fi

docker run --rm -it \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  ${PROJECT_DIR_MAPPING:+-v "$PROJECT_DIR_MAPPING"} \
  ${PROJECT_DATA_MAPPING:+-v "$PROJECT_DATA_MAPPING"} \
  ${EXPERIMENTS_MAPPING:+-v "$EXPERIMENTS_MAPPING"} \
  -e DISPLAY="$ip":0 \
  "${IMAGE}:${TAG}"
