# Project Architecture

# Overview

This document aims to explain the project's architecture design along with any major patterns, principles, and decisions involved in its creation.

# High-Level Architecture

**Note:** Not all fields, methods, and relationships are depicted in this diagram. Its purpose is just to give a high-level overview of the system and the major relationships between its subsystems. Refer to the [PlantUML class diagram documentation](https://plantuml.com/class-diagram) to understand the syntax used.

\startuml
!include ../docs/architecture.puml
\enduml

# Subsystems

## Main

\startuml
!include ../docs/readmanager.puml
\enduml

The **MAIN** subsystem handles program parameters and orchestrates the other components of the system in a composable fashion. The `RunManager` class acts as a central source of truth for application-wide state and behaviour as per the [Mediator Pattern](https://refactoring.guru/design-patterns/mediator).

## Reader

\startuml
!include ../docs/reader.puml
\enduml

The **READER** subsystem handles extracting and parsing input event data. The `Reader`  implements the [Iterator](https://refactoring.guru/design-patterns/iterator) pattern, exposing a device-agnostic stream of events to the mediator. Device-specific reader classes can be written, and the [Factory](https://refactoring.guru/design-patterns/factory-method) class returns the implementation matching the runtime configuration.

## Filtering

\startuml
!include ../docs/filtering.puml
\enduml

The **FILTERING** subsystem filters waveforms as a pre-processor before analysis of the data.

## Baseline Processing

\startuml
!include ../docs/baseline_processing.puml
\enduml

## Pulse Finding

\startuml
!include ../docs/pulse_finding.puml
\enduml

The **PULSE FINDING** subsystem analyzes channel waveform data and identifies pulses using a specified algorithm. The subsystem implements a [Strategy](https://refactoring.guru/design-patterns/strategy) pattern to allow pulse-finding algorithms to be implemented separately and specified via runtime configuration.

## Pulse Analysis

\startuml
!include ../docs/pulse_analysis.puml
\enduml

The **PULSE ANALYSIS** subsystem is responsible for fitting channel pulse data using a specified algorithm. It implements a [Strategy](https://refactoring.guru/design-patterns/strategy) pattern similar to the `PULSE FINDING` for similar reasons.

## Event Level Analysis

\startuml
!include ../docs/event_analysis.puml
\enduml

## Writer

\startuml
!include ../docs/writer.puml
\enduml

# Programming Patterns

## MEDIATOR PATTERN

The [Mediator Pattern](https://refactoring.guru/design-patterns/mediator) allows objects to have minimal interaction with each other. Instead, this pattern employs a central object, or the mediator, which the other objects use to indirectly communicate with one another. This means that objects can stay independent of each other while only depending on the mediator. The immediate benefit to this is that you can safely make changes to an object without it having affect multiple other objects. Only the mediator object would be affected. How this is applied to the vwat project is that when changing any of the subsystem, the user will have to worry about how those changes affect RunManager and classes within those subsystems only. Every other subsystem should remain unchanged or will require minimum refactoring.

A real world example is like the air traffic controller of an airport. Airplanes do not normally communicate with one another to determine the schedule for landing. Instead, all airplanes contacts the air traffic controller for direction instead.

## ITERATOR PATTERN

The [Iterator Pattern](https://refactoring.guru/design-patterns/iterator) removes the logic of traversing a collection of data into it's own separate object. The immediate benefit of such a pattern is that each iterator object can act independently of each other. If you were to traverse the same collection of data twice, you can create two iterator objects to do so. Both will retain their current place during traversal and you'd be able to freely call next on whichever of the iterator pattern you so desire. How this is applied to the vwat project is that the method to traverse a waveform has been removed into it's own Reader class. Instead of having reader login inside the other classes, you can just make a reference to Reader and use the methods there. This improves readibility of the entire codebase.

A real world example is like visiting a new city as a tourist. You want to see all the sights but are unsure of what's the best way to traverse the city. You can download an app online that can take you around the city. Alternatively, you can hire a tour guide that will lead you to the sites themselves. You can try to be romantic and allow your instincts to guide you. You can even use all three or in any combination of your choice. All these are cases of an iterator pattern, the traversal over a collection of data or, in this case, tourist sites.

## FACTORY PATTERN

The [Factory Pattern](https://refactoring.guru/design-patterns/factory-method) is a powerful creatonal design pattern that allows for incredible flexibility in a program. It works by replacing all direct object construction calls with calls to a factory. The factory is a method that will return an object with an interface that will work seamlessly with the program. The benefit of this is that you can change the factory method to construct an object to suit your changing needs without it ever affecting or cluttering all the other objects in your program, so long as the interface is maintained. How this is applied to the vwat project is that you can choose which implementation to run through configurations. This is achievable by loading an object constructor (Factory) which connects to the RunManager. The Factory will return the implementation designated in the configuration and the RunManager will process based upon that implementation without fault. This way, you can load several types of DataFile, V1730 and Lecroy, without having to worry about the RunManager or any of the other subsystems. You can even expand the types of DataFile when necessary as well. Keep in mind, you will have to create corresponding Factories (all with the same DataFileFactory implementation) so that they are compatible with the RunManager class.

A real world example is creating logistics for a transportation firm. You begin by delivering by trucks and figure out all the routes and overhead it takes to do so. Now, your company expands and it wants to transport through boats. You perform the same logic as with trucks, except you figure out sea routes and overhead for a fleet of boats. If you coded this, rather than an if statement that calls the construction of a boat or truck, you'd call a factory to create an object. The logic of the factory, be it trucks or boats, will depend on a prior configuration. This way, your transport object will not have to contain methods belonging to truck or boat. It'll just use what the factory provides.

## STRATEGY PATTERN

The [Strategy Pattern](https://refactoring.guru/design-patterns/strategy) separates family of algorithms into separate classes. Each class with have a common interface, making them interchangeable with one another. The immediate benefit is that you can slot whichever algorithm works at that current time into your program and have it run perfectly fine. How this is applied to the vwat project is that you can choose switch between different types of algorithms in which to examine the waveform. This is easily seen in the pulse finding section. If there comes a time when you want to try a different pulse finding algorithm, so long as you have the correct implementation, specifying the change in configuration will allow the program to run using that specific algorithm.

A real world example would be equipments in a video game. A warrior can use sword, axe, spear, etc. They all possess the ability to attack but, depending on the weapon, the way they attack and, subsequently do damage, will differ from one another.

## REFERENCES

Most pattern's description are a shortened/high level description paraphrased from [Refactoring Guru](https://refactoring.guru/).
