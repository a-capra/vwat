# Docker

# Why Docker?

[Docker](https://docs.docker.com/) is a software platform used for working with [containers](https://www.docker.com/resources/what-container). We use Docker in this project to provide a consistent runtime environment for `VWAT`, allowing us to build, develop, and run it anywhere. Running our software in Docker opens up the development workflow and reduces the dependency on a central server. Of course, Docker environments are just Linux, so everything stays compatible.

# Installing Docker

Refer to the [official Docker instructions](https://docs.docker.com/get-docker/) to set Docker up on your system. Docker runs natively on Linux, but Windows and macOS have integrated Docker toolkits available that make running Docker on those platforms very easy as well.

After Docker is installed, follow [this 2-minute guide](https://docs.docker.com/get-started/#test-docker-installation) to test the installation.

# Our container images

A Docker [container](https://docs.docker.com/glossary/#container) is a small virtualized Linux environment that is based off of a container [image](https://docs.docker.com/glossary/#image), which acts like a blueprint. The image is defined through a [Dockerfile](https://docs.docker.com/engine/reference/builder/), which is just a list of commands that run one by one to configure the environment inside the container.

Images can be hosted publicly or privately in a [registry](https://docs.docker.com/glossary/#registry), which is just a specialized kind of repository. Our images are hosted in [VWAT GitLab's container registry](https://gitlab.com/vwat/vwat/container_registry). This registry is publicly readable as of Nov. 2020, but the "Quick Start" button in the top-right corner of the UI contains instructions for how to authenticate to it if access is ever limited.

Images are given [tags](https://docs.docker.com/glossary/#tag) that can be used to specify different versions of the same base image. There are many tagging conventions, but our image tags are based on the name of the Git branch from which they were built.

## Main image

The main `VWAT` image, defined in the project `Dockerfile`, packages the project's source code, binaries, and dependencies -- everything you need to run or develop the software. It's hosted in VWAT's GitLab's registry. Our utility scripts and automation pipeline handle everything related to pulling and pushing this image, so users should not have to deal with it directly.

This image is based on the [official ROOT container image](https://hub.docker.com/r/rootproject/root), which has some helpful documentation about enabling graphics support on various platforms.

## Utility image(s)

The `docgen` image, defined in `Dockerfile.docgen` also found in our registry in GitLab, is just a utility image that contains all of the dependencies needed to generate [our documentation](docs/DOCUMENTATION.md) with Doxygen. GitLab manages the documentation site through our automation pipeline, so developers should not have to worry about this image.

# Our scripts

We've included a handful of utility scripts to make working with Docker seamless for beginner users. As a user, you should not have to worry about Docker after you install it. As a developer, you should not have to worry about Docker unless you're adding or changing project dependencies.

**Note**: Scripts prefixed with '`ci`' are intended to be used by our automation pipeline. They're just thin wrappers around our `Makefile` that tweak the output to be more useful in the context of the pipeline log view.

## Build

Our build script is located at `scripts/docker-build.sh`. This script runs on both macOS and Linux to build a container image based off of your current Git branch. Docker uses a [cache](https://thenewstack.io/understanding-the-docker-cache-for-faster-builds/) to speed up builds, so builds should only take up to a minute or so.

If you change the project `Makefile`, you may need to run this script with the `--rebuild` tag to bypass the build cache and do a clean build. Note that this will take much longer (~10 minutes).

## Run

We have two run scripts, one for macOS and one for Linux. They do the same thing, but we separated them due to minor platform differences.

The run scripts enable X forwarding from the container to the host so you can use the ROOT browser and other graphical features you may need for your work. If the instructions below do not work for you, refer to the [ROOT documentation](https://hub.docker.com/r/rootproject/root) for platform-specific instructions to set this up.

The run scripts will fetch the latest container image built from your current working branch. This makes it easy to run the version of the project you're currently working on. If you'd prefer to run a more stable build from a branch like `master`, simply check out that branch and run the script from there. If your working branch has not yet been built into a container image, the script will default to `master`. Note that this script is written to work seamlessly with our automation pipeline, which automatically builds and maintains an updated image from every branch pushed to our GitLab repo.

Docker supports [bind mounts](https://docs.docker.com/storage/bind-mounts/), which let containers access files from the host. By default (with no flags), the run scripts will mount the `ntp/`  and `experiments/` directories from the host inside the container. Any changes the code running in the container makes to those directories are persisted in the host's filesystem, so your data analysis results will be accessible after the container is removed. Any changes made to contents of the `expermients/` directory on the host's file system will exists inside the container. The user can modify or add new configuration files and the changes will also be made inside the container. **Note**: If specifying "OutputFilePath" in the JSON configuration file, only output the data to the `ntp/` directory or a subdirectory to access the file on the host's filesystem.

The optional `-p` flag allows a user to specify an absolute path to a directory inside the host's filesystem to be mounted and used inside the container. If data for an experiment exists outside of the `ntp/` directory, simply use `-p <pathtodata>` such as `-p /Users/john/midasfiles`, and the contents of the directory will be mounted to `/home/vanwftk/data` inside the container. **Note**: If specifying "OutputFilePath" in the JSON configuration file, only output the data to the `/home/vanwftk/data` directory or a subdirectory to access the file on the host's filesystem.

One very useful feature of the run scripts for developers is the optional `-d` flag. Using the bind mount feature described above, this flag makes the script mount your entire `vwat/` project directory inside the container, not just the data and experiments directories. This means that you can edit code on your host machine in your favourite editor and the changes will be instantly available to the container -- recompiling your program is as simple as running `make` in the container's shell. **Note**: If specifying "OutputFilePath" in the JSON configuration file, output the data to the `vwat/` directory or a subdirectory to access the file on the host's filesystem.

### Linux

1. Make sure you are in an X11 session
2. Run `./scripts/docker-run-linux.sh` (optionally with `-p <pathtodata>` and/or `-d`)

### OSX

1. Make sure [XQuartz](https://www.xquartz.org/) is installed (necessary for X11 support)
2. Open XQuartz
3. Go to XQuartz > Preferences > Security
4. Tick the box "Allow connections from network clients"
5. Exit XQuartz
6. Run `./scripts/docker-run-osx.sh` (optionally with `-p <pathtodata>` and/or `-d`)

### Windows

1. Make sure [Xming](https://sourceforge.net/projects/xming/) is installed (necessary for X11 support)
2. Whitelist Xming in the Windows firewall (Private and Public)
3. Restart Xming
4. Run `./scripts/docker-run-windows.ps1` (optionally with `-p <pathtodata>` and/or `-d`)

# Resources

**Note**: Docker is used for all sorts of complex, scalable production deployments -- our use case is quite simple so you can get up to speed just by learning a few basic commands

- [Docker documentation](https://docs.docker.com/)
- [Docker tutorial](https://www.docker.com/101-tutorial)
- [3rd-party Docker introduction](https://docker-curriculum.com/)
